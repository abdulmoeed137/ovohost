@extends('layouts.app')

@section('title', 'Payments | OvoHost')

@section('styles')
    <style>
        #leftpay {
            float: left;
            width: 49%
        }

        #rightpay {
            float: right;
            width: 49%
        }

        @media only screen and (max-width: 767px) {
            #leftpay {
                float: none;
                width: 100%
            }

            #rightpay {
                float: none;
                width: 100%
            }
        }

        div.tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            margin-top: 10px;
        }

        div.tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        div.tab button:hover {
            -webkit-background-clip: no-clip;
            -moz-background-clip: no-clip;
            background-clip: no-clip;
            color: #ddd;
        }

        div.tab button.active {
            background-color: #ccc;
        }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
            margin-bottom: 20px;
        }

        .headform {
            margin-bottom: 5px;
            margin-top: 5px;
            font-size: 14px;
            font-weight: bold
        }

        .field3 {
            border: 1px solid #bab9b9;
            padding: 5px;
            width: 250px;
            color: #000;
            font-size: 15px;
        }

        .com_btn {
            margin: 0px;
            padding: 5px 20px;
            border: 0px;
            background-color: #1b4f6d;
            color: #ffffff;
            font-size: 16px;
            margin-top: 10px
        }

    </style>
@endsection

@section('content')
    <div id="headline">
        <h1>Payments</h1>
    </div>
    <div class="content-adj">
        <h3 style="text-align: center; margin-bottom: 20px; font-size: 27px;">Payment Options & Account Activation</h3>

        <p style="text-align: center; border-bottom: 1px solid #ccc; padding-bottom: 10px; margin-bottom: 20px">
            Once you have submitted your order, please follow below 2 steps to get your account activated within 30 minutes.
        </p>

        <div id="leftpay">

            <p style="text-align: center">
                <strong>Step # 1: Submit Payment</strong>
                <br>
                <span style="font-size: 15px">Submit your payment using any of the below mentioned payment methods. If you
                    don't find a right payment option, please
                    <a href="contact.html">contact us</a>.</span>
            </p>

            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'bank')" id="defaultOpen">Bank Payments</button>
                <button class="tablinks" onclick="openCity(event, 'mobile')">Mobile Payments</button>
                <button class="tablinks" onclick="openCity(event, 'other')">Other Methods</button>
            </div>

            <div id="bank" class="tabcontent">
                <p>You can send payments into below below mentioned accounts:</p>
                <ul style="margin-left: 20px; margin-bottom:20px; margin-top: 10px">
                    <li style="margin-bottom:10px">Bank: UBL Bank<br>Title: OvoHost<br>A/C: 0932-246760863<br>IBAN:
                        PK20UNIL0109000246760863</li>
                </ul>
                <p>Incase of cash deposit, please confirm charges with bank at the time of payment.</p>
            </div>

            <div id="mobile" class="tabcontent">
                <p>You can send payments to any of the below mentioned mobile accounts</p>
                <ul style="margin-left: 20px; margin-bottom:20px; margin-top: 10px">
                    <li style="margin-bottom:10px">Jazzcash to Bank Account<br>Bank: UBL Bank<br>A/C:
                        0932-246760863<br>Title: OvoHost<br>Mobile: 0300-8236650<br /></li>
                    <li style="margin-bottom:10px">Easypaisa to Bank Account<br>Bank: UBL Bank<br>A/C:
                        0932-246760863<br>Title: OvoHost<br>Mobile: 0300-8236650<br /></li>
                    <li style="margin-bottom:10px">OMNI to Bank Account<br>Bank: UBL Bank<br>A/C: 0932-246760863<br>Title:
                    OvoHost<br>Mobile: 0300-8236650<br /></li>
                    <li style="margin-bottom:10px">UPaisa to Bank Account<br>Bank: UBL Bank<br>A/C: 0932-246760863<br>Title:
                    OvoHost<br>Mobile: 0300-8236650<br /></li>
                </ul>
                <p>If you need any help, please reach us on Whatsapp @ 0300-8236650 for help.</p>
            </div>

            <div id="other" class="tabcontent">
                <p>You can make payment using below mentioned methods:</p>
                <ul style="margin-left: 20px; margin-bottom:20px; margin-top: 10px">
                    <li>Credit / Debit cards</li>
                    <li>Skrill</li>
                    <li>Paypal</li>
                </ul>
                <p>Please send email to sales@ovohost.pk to obtain instructions of the above payment methods.</p>
            </div>

        </div>

        <div id="rightpay">

            <p style="text-align: center" id="payment-confirmation">
                <strong>Step # 2: Send Payment Confirmation</strong>
                <br>
                <span style="font-size: 15px">Once your payment is complete, fill below form to send payment details for
                    account activation.</span>
            </p>

            <div id="abc"
                style="padding: 20px; border: 1px solid #ccc; margin-bottom: 20px; text-align: center; background: #f1f1f1; margin-top: 10px">

                <h3>Send Payment Details For Account Activation</h3>

                <form action="https://www.easyhost.pk/payments?submit=true#payment-confirmation" method="post"
                    style="margin-top: 10px; margin-bottom: 10px">
                    <p class="headform">Email (registered email address):</p>
                    <p><input type="email" name="email" class="field3" value="" /></p>
                    <p class="headform">Invoice Number:</p>
                    <p><input type="number" name="invoice" class="field3" value="" /></p>
                    <p class="headform">Payment Method:</p>
                    <p><select name="method" class="field3">
                            <option value="" selected>-- Select Option --</option>
                            <option value="UBL">United Bank Limited</option>
                            <option value="Jazzcash to Bank">Jazzcash to Bank</option>
                            <option value="Easypaisa to Bank">Easypaisa to Bank</option>
                            <option value="OMNI to Bank">OMNI to Bank</option>
                            <option value="UPaisa to Bank">UPaisa to Bank</option>
                        </select>
                    </p>
                    <p class="headform">Transaction Number:</p>
                    <p><input type="number" name="trx" class="field3" value="" /></p>
                    <p class="headform">Amount Sent:</p>
                    <p><input type="number" name="amount" class="field3" value="" /></p>
                    <p class="headform">Date of Payment:</p>
                    <p><input type="date" name="date" class="field3" value="" /></p>
                    <p class="headform">Verification: what is 2 + 4? type below:</p>
                    <p><input type="hidden" name="verify" value="6" />
                        <input type="number" name="vernum" class="field3" placeholder="Your answer here" />
                    </p>
                    <p><input name="submit" type="submit" id="submit" tabindex="5" class="com_btn"
                            value="Confirm My Payment" /><br />
                </form>

                <p><i>Note: please ensure correct details are filled in to avoid delays.</i></p>

            </div>

        </div>

        <div class="clear"></div>


        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>
        <div class="clear"></div>
    </div>
@endsection

@section('scripts')
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        document.getElementById("defaultOpen").click();
    </script>
@endsection
