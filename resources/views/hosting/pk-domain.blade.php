@extends('layouts.app')

@section('title', 'Free .COM Domain Registration with Web Hosting Saudi Arabia | OVOHost')

@section('styles')
    <style>
        .domainsbox {
            width: 21%;
            float: left;
            background-color: #f9f9f9;
            margin-left: 2%;
            margin-right: 2%;
            margin-top: 10px;
            margin-bottom: 20px
        }

        .domainscircle {
            margin: 10px auto;
            border-radius: 50%;
            background: #0e5077;
            width: 150px;
            height: 150px;
            text-align: center;
            color: white;
            font-weight: bold;
        }

        .domainscircle h3 {
            padding-top: 50px;
            font-size: 22px
        }

        .domainscircle p {
            font-size: 15px !important;
        }

        .domainsbox p {
            text-align: center;
            padding: 0px 10px;
            font-size: 13px;
        }

        .lfamous {
            padding: 5px;
            background-color: darkorange;
            font-weight: bold;
            color: white
        }

        .searchref {
            padding: 5px;
            background-color: #0e5077;
            color: white;
            text-decoration: none
        }

        @media only screen and (max-width : 767px) {
            .domainsbox {
                width: 100%
            }
        }

    </style>
@endsection

@section('content')
    <div id="headline">
        <h1>Web Hosting with Free .COM Domain</h1>
    </div>

    <div class="content-adj">
        <p>For the first time in Saudi Arabia, OvoHost is offering <strong>Web Hosting with Free .COM Domain Name & SSL
                certificate</strong> with its COM Domain Hosting service packages. All plans are billed bi-annually so you
            get 2 years of reliable <strong>.com domain hosting in Saudi Arabia</strong> at affordable price!</p>

        <div id="packages">
            <div class="pricebox pricebox-a">
                <div class="pricehead">COM-Host I</div>
                <div class="amount">SAR 8,000 (2 years)</div>
                <ul>
                    <li>1500 MB Diskspace</li>
                    <li>25 GB Bandwidth</li>
                    <li class="red">Free .COM Domain<br>(.COM)</li>
                    <li class="red">Free SSL Certificate (Save $10)</li>
                    <li><strong>Host 1 Domain</strong></li>
                    <li>25 Email Accounts</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                    <li>Free Website Builder</li>
                    <li><a href="#features">More Details</a></li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=18" class="order-b">Order
                        Now!</a>
                    <!-- <a href="account/cart8913.html?a=add&amp;pid=18&amp;source=pkhosting" class="order-b">Order
                        Now!</a> -->
                        <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-b">Order Now!</a>-->

                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">COM-Host II</div>
                <div class="amount">SAR 9,000 (2 years)</div>
                <ul>
                    <li>2500 MB Diskspace</li>
                    <li>50 GB Bandwidth</li>
                    <li class="red">Free .COM Domain<br>(.COM)</li>
                    <li class="red">Free SSL Certificate (Save $10)</li>
                    <li><strong>Host 2 Domains</strong></li>
                    <li>50 Email Accounts</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                    <li>Free Website Builder</li>
                    <li><a href="#features">More Details</a></li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=19" class="order-b">Order
                        Now!</a>
                    <!-- <a href="account/carta59b.html?a=add&amp;pid=19&amp;source=pkhosting" class="order-b">Order
                        Now!</a> -->
                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">COM-Host III</div>
                <div class="amount">SAR 10,000 (2 years)</div>
                <ul>
                    <li>3500 MB Diskspace</li>
                    <li>75 GB Bandwidth</li>
                    <li class="red">Free .COM Domain<br>(.COM)</li>
                    <li class="red">Free SSL Certificate (Save $10)</li>
                    <li><strong>Host 3 Domains</strong></li>
                    <li>75 Email Accounts</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                    <li>Free Website Builder</li>
                    <li><a href="#features">More Details</a></li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=20" class="order-b">Order
                        Now!</a>
                    <!-- <a href="account/carte7e6.html?a=add&amp;pid=20&amp;source=pkhosting" class="order-b">Order
                        Now!</a> -->
                </p>
            </div>

            <div class="pricebox" style="border-color:#f4590b !important">
                <div class="pricehead" style="background:#e38d36 !important">COM-Unlimited I</div>
                <div class="amount">SAR 12,000 (2 years)</div>
                <ul>
                    <li>Unlimited Diskspace</li>
                    <li>Unlimited Bandwidth</li>
                    <li class="red">Free .COM Domain<br>(.COM)</li>
                    <li class="red">Free SSL Certificate (Save $10)</li>
                    <li><strong>Host 1 Domain</strong></li>
                    <li>Unlimited Email Accounts</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                    <li>Free Website Builder</li>
                    <li><a href="#features">More Details</a></li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=21" class="order-b">Order
                        Now!</a>
                    <!-- <a href="account/cart2292.html?a=add&amp;pid=21&amp;source=pkhosting" class="order-b">Order
                        Now!</a> -->
                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">COM-Unlimited II</div>
                <div class="amount">SAR 18,000 (2 years)</div>
                <ul>
                    <li>Unlimited Diskspace</li>
                    <li>Unlimited Bandwidth</li>
                    <li class="red">Free .COM Domain<br>(.COM)</li>
                    <li class="red">Free SSL Certificate (Save $10)</li>
                    <li><strong>Host Unlimited Domains</strong></li>
                    <li>Unlimited Email Accounts</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                    <li>Free Website Builder</li>
                    <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=22" class="order-b">Order
                        Now!</a>
                    <!-- <a href="account/cart5db1.html?a=add&amp;pid=22&amp;source=pkhosting" class="order-b">Order
                        Now!</a> -->
                </p>
            </div>

        </div>

        <div class="clear"></div>

        <section class="feature-block">
            <h3 style="text-align: center; margin-bottom: 15px; font-size: 20px">
                Our web hosting plans include 100+ free scripts for you. Some open source scripts that our clients love:
            </h3>

            <div class="qtr-service">
                <img src="{{ asset('images/wordpress.png') }}" alt="wordpress" />
                <h4>Wordpress</h4>
                <p>World's leading framework for Blogs & Corporate Websites.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/joomla.png') }}" alt="joomla" />
                <h4>Joomla</h4>
                <p>PHP framework for creating websites with easy to us admin portal.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/drupal.png') }}" alt="drupal" />
                <h4>Drupal</h4>
                <p>PHP based opensource CMS for creating websites and web applications.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/smf.png') }}" alt="SMF Forum" />
                <h4>SMF Forums</h4>
                <p>SMF Forums help you start your own forums in just few clicks.</p>
            </div>

            <div class="clear"></div>

            <div class="qtr-service">
                <img src="{{ asset('images/opencart.png') }}" alt="Opencart" />
                <h4>OpenCart</h4>
                <p>Fastest way to take your store online live today in few clicks.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/prestashop.png') }}" alt="prestashop" />
                <h4>PrestaShop</h4>
                <p>Popular platform to make an online shopping website for your products.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/magento.png') }}" alt="magento" />
                <h4>Magento</h4>
                <p>Popular for its eCommerce functionality, security and scalability.</p>
            </div>

            <div class="qtr-service">
                <img src="{{ asset('images/osCommerce.png') }}" alt="osCommerce" />
                <h4>osCommerce</h4>
                <p>Another eCommerce tool to make your shop get online on the website.</p>
            </div>

            <div class="clear"></div>
        </section>

        <div class="clear"></div>

        <h3
            style="text-align: center; font-size: 25px; padding-top: 20px; margin-top:20px; margin-bottom: 20px; border-top:1px solid #333">
            Why choose .COM Domain Name</h3>

        <p style="text-align: center; margin-bottom: 20px">
            .COM Domain is recommended for all Saudi Arabia based websites i.e. any blog publishing Saudi Arabia's content or business
            targeting local customers should register .COM domain. You can take our example, Ovohost is the best COM domain
            hosting company based in Karachi and therefore it has registered .COM domain for corporate website.
            <br><br>
            General registration guide for all available .COM extensions are given below:
        </p>

        <div class="domainsbox">
            <p class="lfamous">Most Used Extension!</p>
            <div class="domainscircle">
                <h3>.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Most Used Extension!</p>
            <div class="domainscircle">
                <h3>.COM.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Best for Network & ISPs</p>
            <div class="domainscircle">
                <h3>.NET.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Networking bodies & ISPs.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Best for Organizations / NGOs</p>
            <div class="domainscircle">
                <h3>.ORG.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Organizations & Not-for-Profit cause.</p>
        </div>

        <div class="clear"></div>

        <div class="domainsbox">
            <p class="lfamous">General / Business Use</p>
            <div class="domainscircle">
                <h3>.BIZ.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">General / Business Use</p>
            <div class="domainscircle">
                <h3>.WEB.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">For Edu. Institutes</p>
            <div class="domainscircle">
                <h3>.EDU.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>For registered institutes to register.<br>Registered academies / schools and education centers use.</p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">For Gov. Organizations</p>
            <div class="domainscircle">
                <h3>.GOV.COM</h3>
                <p>SR.2,200/2yr</p>
            </div>
            <p>For Registerd GOV. Org. to register.<br>Government departments, organizations can register.</p>
        </div>

        <div class="clear"></div>

        <div class="content-adj">
            <div id="faq">

                <h3 style="margin-bottom: 10px">Got questions regarding .COM Domain Hosting?</h3>

                <p class="accordion">How can i register COM domain hosting?</p>
                <div class="panel">
                    Select any package given above and search available free COM domain of your choice at the next step. In a
                    couple minutes you will be able to complete your order for COM domain hosting online.
                </div>

                <p class="accordion">What if i need .COM Domain registration without hosting?</p>
                <div class="panel">
                    No worries, Ovohost offer cheap rates for .COM domain registration, take a look at our <a
                        href="pk-domains.html">.COM domain prices</a>.
                </div>

                <p class="accordion">Why choose Ovohost.com for .COM?</p>
                <div class="panel">
                    Ovohost is #1 .COM Domain provider in Saudi Arabia, if you need .COM domain registered for your website - you
                    have landed in the right place!
                </div>


            </div>

            <script>
                document.addEventListener("DOMContentLoaded", function(event) {


                    var acc = document.getElementsByClassName("accordion");
                    var panel = document.getElementsByClassName('panel');

                    for (var i = 0; i < acc.length; i++) {
                        acc[i].onclick = function() {
                            var setClasses = !this.classList.contains('active');
                            setClass(acc, 'active', 'remove');
                            setClass(panel, 'show', 'remove');

                            if (setClasses) {
                                this.classList.toggle("active");
                                this.nextElementSibling.classList.toggle("show");
                            }
                        }
                    }

                    function setClass(els, className, fnName) {
                        for (var i = 0; i < els.length; i++) {
                            els[i].classList[fnName](className);
                        }
                    }

                });
            </script>

        </div>
        <div class="clear"></div>

        <p style="line-height: 30px; text-align: center">
            Need help? <a href="{{ route('contact') }}">Contact Us Now!</a>
        </p>

        <div class="clear"></div>

        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>

        <div class="clear"></div>
    </div>
@endsection
