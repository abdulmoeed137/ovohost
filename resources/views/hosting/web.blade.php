<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


@extends('layouts.app')

@section('title', 'Web Hosting Saudi Arabia | Domain Hosting Jeddah | OvoHost')


@section('styles')
<style>
#features_new {
    width: 100%;
    margin-bottom: 20px;
    margin-top: 20px;
}

#features_new h3 {
    padding: 10px 40px
}

#features_new ul {
    margin-left: 70px;
    margin-right: 50px
}

#features_new ul li {
    float: left;
    width: 25%;
    list-style-image: url('images/ricon.png');
    line-height: 40px;
}

#features_block {
    margin: 0 auto;
    width: 90%
}

.feature_box {
    width: 33.3%;
    text-align: center;
    float: left;
    margin-bottom: 30px
}

.feature_box p {
    font-size: 14px
}

.int_width {
    width: 240px;
    margin: 0 auto;
}

@media only screen and (max-width: 767px) {
    #features_block {
        margin: 0 auto;
        width: 100%
    }

    .feature_box {
        width: 100%;
        text-align: center;
        float: none;
        margin-bottom: 30px
    }

    .feature_box p {
        font-size: 14px
    }

    .int_width {
        width: 80%;
        margin: 0 auto
    }
}
.container{
    padding: 0;
}
</style>
@endsection




@section('content')
<div id="headline" style="margin: 0">
    <h1 style="margin: 0; padding-top: 22px;">Web Hosting Saudi Arabia</h1>
</div>
</br>
<!-- ---------------------------Start OF Tab Labels---------------------------------------- -->
{{-- <ul class="nav nav-pills">
    <li class="active">
        <a data-toggle="pill" href="#webhosting">
            <h4><b>Web Hosting</b></h4>
        </a>
    </li>
    <li>
        <a data-toggle="pill" href="#unlimitedhosting">
            <h4><b>Unlimited Hosting</b></h4>
        </a>
    </li>
</ul> --}}

<div class="content-adj">
    <ul class="tab-in">
        <li class="active">
            <a href="{{ route('hosting') }}">
                <b>Web Hosting</b>
            </a>
        </li>
        <li>
            <a href="{{ route('host', 'unlimited') }}">
               <b>Unlimited Hosting</b>
            </a>
        </li>
    </ul>
    
    <!-- ---------------------------End OF Tab Labels---------------------------------------- -->
    
    <!-- ---------------------------Start OF Tab Content---------------------------------------- -->
    
    <div class="tab-content">
        <div id="webhosting" class="tab-pane fade in active">
            <div class="content-adj">
                <h2>Web Hosting Packages</h2>
                <p>
                    Ovohost is Saudi Arabia’s top rated Web Hosting Company, offering reliable Budget Web Hosting packages
                    including
                    Free Domain Registration & SSL certificates at an affordable price!
                    Take a look our web hosting packages below:
                </p>
                <div id="table">
                    <table width="100%" cellpadding="0" cellspacing="0" id="plansblock"
                        style="margin-top: 20px; margin-bottom:20px">
                        <thead>
                            <td width="25%" class="lalign">Plans</td>
                            <td width="15%">Budget I</td>
                            <td width="15%">Budget II</td>
                            <td width="15%">Budget III</td>
                            <td width="15%">Unlimited I</td>
                            <td width="15%">Unlimited II</td>
                        </thead>
                        <tr>
                            <td width="25%" class="lalign">Disk Space</td>
                            <td width="15%">1500 MB</td>
                            <td width="15%">2500 MB</td>
                            <td width="15%">3500 MB</td>
                            <td width="15%">Unlimited</td>
                            <td width="15%">Unlimited</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Bandwidth</td>
                            <td width="15%">25 GB</td>
                            <td width="15%">50 GB</td>
                            <td width="15%">75 GB</td>
                            <td width="15%">Unlimited</td>
                            <td width="15%">Unlimited</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Free Domain</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Free SSL</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">cPanel</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Email Accounts</td>
                            <td width="15%">50</td>
                            <td width="15%">100</td>
                            <td width="15%">150</td>
                            <td width="15%">Unlimited</td>
                            <td width="15%">Unlimited</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Sub-domains</td>
                            <td width="15%">10</td>
                            <td width="15%">10</td>
                            <td width="15%">10</td>
                            <td width="15%">Unlimited</td>
                            <td width="15%">Unlimited</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Domains Allowed</td>
                            <td width="15%">1</td>
                            <td width="15%">2</td>
                            <td width="15%">3</td>
                            <td width="15%">1</td>
                            <td width="15%">10</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">MySQL database</td>
                            <td width="15%">3</td>
                            <td width="15%">4</td>
                            <td width="15%">5</td>
                            <td width="15%">Unlimited</td>
                            <td width="15%">Unlimited</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign"><strong>Annual Fee</strong></td>
                            <td width="15%"><strong>SR.3,000/yr</strong></td>
                            <td width="15%"><strong>SR.3,750/yr</strong></td>
                            <td width="15%"><strong>SR.4,500/yr</strong></td>
                            <td width="15%"><strong>SR.6,000/yr</strong></td>
                            <td width="15%"><strong>SR.9,000/yr</strong></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign"></td>
                            <td width="15%">
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-btn">Order</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td width="15%">
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=2" class="order-btn">Order</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=2" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td width="15%">
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=5" class="order-btn">Order</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=5" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td width="15%">
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-btn">Order</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td width="15%">
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-btn">Order</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-b">Order Now!</a>-->
    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><strong>Other Web Hosting Features</strong></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Datacenter</td>
                            <td width="15%">Saudi Arabia</td>
                            <td width="15%">Saudi Arabia</td>
                            <td width="15%">Saudi Arabia</td>
                            <td width="15%">Saudi Arabia</td>
                            <td width="15%">Saudi Arabia</td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">24/7 Support</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">99.9% Uptime</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">MoneyBack Guarantee</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">WordPress Hosting</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">phpMyAdmin</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Softaculous &#8211; <a
                                    href="http://demo.softaculous.com/enduser/">Demo</a></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td width="25%" class="lalign">Audio Streaming</td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td width="15%"><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                    </table>
                </div>
                <div id="features" style="margin-top: 20px">
                    <h3>Web Hosting Features</h3>
                    <ul>
                        <li>Free domain registration</li>
                        <li>99.99% uptime delivery</li>
                        <li>30 days money back guarantee</li>
                        <li>24x7 sms, whatsapp & email support</li>
                    </ul>
                    <div class="clear"></div>
                    <p style="margin: 5px 0px"><strong>and...</strong></p>
                    <ul>
                        <li>cPanel control panel</li>
                        <li>PHP, MySQL, PHP myAdmin</li>
                        <li>Personalized email accounts</li>
                        <li>Self backup functions</li>
                        <li>FTP user accounts</li>
                        <li>File manager</li>
                        <li>Password protected directories</li>
                        <li>Detailed website statistics</li>
                        <li>Wordpress supported</li>
                        <li>Magento supported</li>
                        <li>Ecommerce ready</li>
                        <li>Mailing lists</li>
                        <li>1 click scripts installer</li>
                        <li>Free site builder</li>
                        <li>Auto backup utility</li>
                        <li>100+ powerful scripts pack</li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div id="features_new">
                    <h3 style="text-align: center; margin-bottom: 30px; font-size: 20px;">Great features with all web
                        hosting
                        plans</h3>
                    <div id="features_block">
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/domains.png') }}" alt="" width="80">
                                <p><strong>Free Domain Names</strong></p>
                                <p>We offer free domain name with web hosting plans</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/site-security.png') }}" alt="" width="80">
                                <p><strong>Free SSL Certificates</strong></p>
                                <p>We take site security serious! Offering free SSL with all plans</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/uptime.png') }}" alt="" width="80">
                                <p><strong>99.9% Uptime</strong></p>
                                <p>We guarantee 99.9% uptime on all web hosting plans</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/website-builder.png') }}" alt="" width="80">
                                <p><strong>Website Builder</strong></p>
                                <p>Our hosting panel includes website builder so ou can start in no time</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/e-commerce.png') }}" alt="" width="80">
                                <p><strong>E-Commerce</strong></p>
                                <p>Our hosting plans supports Ecommerce websites!</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/ideas.png') }}" alt="" width="80">
                                <p><strong>100+ Freebies</strong></p>
                                <p>You get 100+ one click applications and templates for free!</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="block">
                    <h3>Ovohost's mission:</h3>
                    <p>
                        Our mission is to enable Saudi to start their online website with ease. We continuously strive to
                        provide our customers with international standard, best web hosting services, while keeping a cheap
                        price
                        tag (we never compromise stability, server speed & uptime!). If you want your website hosted,
                        Ovohost is
                        the reliable hosting option for you!
                    </p>
                </div>
                <div id="aboutus">
                    <h3>What you get with Ovohost's web hosting services</h3>
                    <p>
                        Ovohost website hosting packages offers Linux based cPanel hosting (world’s most widely used
                        webhosting
                        platform) with Free Domain Registration & SSL Certificate, allowing easy website hosting management
                        to
                        webmasters.
                        With our web hosting control panel, you can create your custom email addresses, setup subdomains and
                        install
                        100+ free open source CMS including wordpress in just few clicks!
                        On top of this, our friendly support team is there to help you 24x7 through Whatsapp, Call, Live
                        Chat &
                        Emails.
                    </p>
                    <h3 style="padding-top:10px; border-top:1px solid #000; margin-top:10px">Get your website online in just
                        30
                        minutes</h3>
                    <p>
                        Yes, this is true - you can order domain and hosting with Ovohost in under 5 minutes. Simply select
                        any
                        hosting plan of your choice and continue with your order, the process is simple, easy and fast!
                        Once you have placed the order, you can make payment through any available payment option and your
                        website
                        will be live in less than 30 minutes.
                    </p>
                    <h3 style="padding-top:10px; border-top:1px solid #000; margin-top:10px">Our committments:</h3>
                    <table width="80%" style="margin: 20px auto;">
                        <tr>
                            <td width="33%" style="text-align: center;"><img
                                    src="{{ asset('images/money-back-guarantee.gif') }}" alt="money back guarantee"
                                    height="140"></td>
                            <td width="33%" style="text-align: center"><img src="{{ asset('images/uptime-guarantee.png') }}"
                                    alt="uptime guarantee" height="140"></td>
                            <td width="33%" style="text-align: center"><img
                                    src="{{ asset('images/satisfaction-guarantee.png') }}" alt="satisfaction guarantee"
                                    height="140"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">30 Days
                                Money Back
                                Guarantee - so you can try hassle free!
                            </td>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">You get
                                99.9% uptime
                                on our Saudi Arabia based servers!
                            </td>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">We go extra
                                mile for
                                customer's satisfaction whenever you need!
                            </td>
                        </tr>
                    </table>
                </div>
    
                <div class="buttonset">
                    <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                    <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING
                        FROM US</a>
                    <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
                </div>
    
                <div class="clear"></div>
    
                <div class="clear"></div>
            </div>
        </div>
        <!-- -----------------------End of Web Hosting Tab content-------------------------- -->
    
        <!-- ------------------------Start of Second Tab Content---------------------------- -->
        <div id="unlimitedhosting" class="tab-pane fade in active">
    
            <div class="content-adj">
                <h2>Unlimited Web Hosting Plans</h2>
                <p>Ovohost is one of the top Unlimited Web Hosting Providers in Saudi Arabia, offering affordable unlimited
                    hosting
                    packages including Free Domain Registration & SSL certificate at an affordable price!</p>
    
                <div id="table">
                    <table width="100%" cellpadding="0" cellspacing="0" id="plansblock"
                        style="margin-top: 20px; margin-bottom:20px">
                        <thead>
                            <td width="50%" class="lalign">Plans</td>
                            <td>Unlimited I</td>
                            <td>Unlimited II</td>
                            <td>Unlimited III</td>
                        </thead>
                        <tr>
                            <td class="lalign">Disk Space</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign">Bandwidth</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign">Free Domain</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">Free SSL</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">cPanel</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">Email Accounts</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign">Sub-domains</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign">Domains Allowed</td>
                            <td>1</td>
                            <td>10</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign">MySQL database</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td class="lalign"><strong>Annual Fee</strong></td>
                            <td><strong>SR.6,000/yr</strong></td>
                            <td><strong>SR.9,000/yr</strong></td>
                            <td><strong>SR.12,000/yr</strong></td>
                        </tr>
                        <tr>
                            <td class="lalign"></td>
                            <td>
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-btn">Order
                                    Now</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td>
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-btn">Order
                                    Now</a>
                                <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-b">Order Now!</a>-->
    
                            </td>
                            <td>
                                <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=44" class="order-btn">Order
                                    Now</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><strong>Included in Unlimited Plans</strong></td>
                        </tr>
                        <tr>
                            <td class="lalign">Datacenter</td>
                            <td>Saudi Arabia</td>
                            <td>Saudi Arabia</td>
                            <td>Saudi Arabia</td>
                        </tr>
                        <tr>
                            <td class="lalign">24/7 Support</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">99.9% Uptime</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">MoneyBack Guarantee</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">WordPress Hosting</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">phpMyAdmin</td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td class="lalign">Softaculous &#8211; <a href="http://demo.softaculous.com/enduser/">Demo</a>
                            </td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                            <td><img src="{{ asset('images/ricon.png') }}" alt="" /></td>
                        </tr>
                    </table>
                </div>
                <div id="features" style="margin-top: 20px">
                    <h3>Web Hosting Features</h3>
                    <ul>
                        <li>Free domain registration</li>
                        <li>99.99% uptime delivery</li>
                        <li>30 days money back guarantee</li>
                        <li>24x7 sms, whatsapp & email support</li>
                    </ul>
                    <div class="clear"></div>
                    <p style="margin: 5px 0px"><strong>and...</strong></p>
                    <ul>
                        <li>cPanel control panel</li>
                        <li>PHP, MySQL, PHP myAdmin</li>
                        <li>Personalized email accounts</li>
                        <li>Self backup functions</li>
                        <li>FTP user accounts</li>
                        <li>File manager</li>
                        <li>Password protected directories</li>
                        <li>Detailed website statistics</li>
                        <li>Wordpress supported</li>
                        <li>Magento supported</li>
                        <li>Ecommerce ready</li>
                        <li>Mailing lists</li>
                        <li>1 click scripts installer</li>
                        <li>Free site builder</li>
                        <li>Auto backup utility</li>
                        <li>100+ powerful scripts pack</li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div id="features_new">
                    <h3 style="text-align: center; margin-bottom: 30px; font-size: 20px;">Great features with all web
                        hosting plans
                    </h3>
                    <div id="features_block">
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/domains.png') }}" alt="" width="80">
                                <p><strong>Free Domain Names</strong></p>
                                <p>We offer free domain name with web hosting plans</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/site-security.png') }}" alt="" width="80">
                                <p><strong>Free SSL Certificates</strong></p>
                                <p>We take site security serious! Offering free SSL with all plans</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/uptime.png') }}" alt="" width="80">
                                <p><strong>99.9% Uptime</strong></p>
                                <p>We guarantee 99.9% uptime on all web hosting plans</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/website-builder.png') }}" alt="" width="80">
                                <p><strong>Website Builder</strong></p>
                                <p>Our hosting panel includes website builder so ou can start in no time</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/e-commerce.png') }}" alt="" width="80">
                                <p><strong>E-Commerce</strong></p>
                                <p>Our hosting plans supports Ecommerce websites!</p>
                            </div>
                        </div>
                        <div class="feature_box">
                            <div class="int_width">
                                <img src="{{ asset('images/ideas.png') }}" alt="" width="80">
                                <p><strong>100+ Freebies</strong></p>
                                <p>You get 100+ one click applications and templates for free!</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
    
                <div id="aboutus">
                    <h3>About Unlimited Hosting in Saudi Arabia</h3>
                    <p>
                        Ovohost's Unlimited website hosting is powered by high end Xeon Servers located in Tier 1
                        Datacenters that
                        deliver 99.9% uptime and top notch speed! With simple and easy to use cPanel, you can start building
                        your
                        website in just few clicks. Want to install Wordpress? Our easy unlimited hosting service include
                        one click
                        installer (Softaculous) to setup Wordpress, Opencart and other open source CMS in under 30 seconds.
                    </p>
    
                    <h3 style='padding-top:10px; border-top:1px solid #000'>Can i buy Unlimited Web Hosting in Jeddah or
                        Riyadh?
                    </h3>
                    <p>
                        Yes, in fact you can buy Unlimited Hosting from anywhere in Saudi Arabia without any hassle! Select
                        any cheap
                        unlimited web hosting plan based on your requirement and complete your order, it takes less than 5
                        minutes.
                        Once you pay your invoice, we will setup your domain and unlimited web hosting services within 15-30
                        minutes.
                    </p>
                    <h3 style='padding-top:10px; border-top:1px solid #000'>Why choose Ovohost's Unlimited Website Hosting?
                    </h3>
                    <ul style="list-style:none; color:green">
                        <li style="padding:5px 0; background:#f5f5f5; font-weight:bold; margin-bottom:5px">Super fast
                            Unlimited
                            Hosting servers in Saudi Arabia</li>
                        <li style="padding:5px 0; background:#f5f5f5; font-weight:bold; margin-bottom:5px">FREE Domain
                            Registration
                            & SSL Certificate</li>
                        <li style="padding:5px 0; background:#f5f5f5; font-weight:bold; margin-bottom:5px">30 Days Money
                            Back
                            Guarantee!</li>
                        <li style="padding:5px 0; background:#f5f5f5; font-weight:bold; margin-bottom:5px">24x7 Livechat,
                            Whatsapp &
                            Email support</li>
                    </ul>
                    <h3 style='padding-top:10px; border-top:1px solid #000'>Our committments</h3>
                    <table width="80%" style="margin: 20px auto;">
                        <tr>
                            <td width="33%" style="text-align: center;">
                                <img src="{{ asset('images/money-back-guarantee.gif') }}" alt="money back guarantee"
                                    height="140">
                            </td>
                            <td width="33%" style="text-align: center">
                                <img src="{{ asset('images/uptime-guarantee.png') }}" alt="uptime guarantee" height="140">
                            </td>
                            <td width="33%" style="text-align: center">
                                <img src="{{ asset('images/satisfaction-guarantee.png') }}" alt="satisfaction guarantee"
                                    height="140">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">30 Days
                                Money Back
                                Guarantee - so you can try hassle free!
                            </td>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">You get
                                99.9% uptime
                                on
                                our Saudi Arabia based servers!
                            </td>
                            <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">We go extra
                                mile for
                                customer's satisfaction whenever you need!
                            </td>
                        </tr>
                    </table>
                </div>
    
                <div class="buttonset">
                    <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
                    <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING
                        FROM US</a>
                    <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST
                        TEAM</a>
                </div>
    
                <div class="clear"></div>
    
                <div class="clear"></div>
            </div>
        </div>
        <!-- ---------------------------End of Second Tab Content--------------------------------- -->
    </div>
</div>




@endsection