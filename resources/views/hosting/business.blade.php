@extends('layouts.app')

@section('title', 'Business Hosting Saudi Arabia | Corporate Website Hosting Packages | OvoHost')

@section('styles')
<style>
                    #features_new {
                        width: 100%;
                        margin-bottom: 20px;
                        margin-top: 20px;
                    }

                    #features_new h3 {
                        padding: 10px 40px
                    }

                    #features_new ul {
                        margin-left: 70px;
                        margin-right: 50px
                    }

                    #features_new ul li {
                        float: left;
                        width: 25%;
                        list-style-image: url('images/ricon.png');
                        line-height: 40px;
                    }

                    #features_block {
                        margin: 0 auto;
                        width: 90%
                    }

                    .feature_box {
                        width: 33.3%;
                        text-align: center;
                        float: left;
                        margin-bottom: 30px
                    }

                    .feature_box p {
                        font-size: 14px
                    }

                    .int_width {
                        width: 240px;
                        margin: 0 auto;
                    }

                    @media only screen and (max-width: 767px) {
                        #features_block {
                            margin: 0 auto;
                            width: 100%
                        }

                        .feature_box {
                            width: 100%;
                            text-align: center;
                            float: none;
                            margin-bottom: 30px
                        }

                        .feature_box p {
                            font-size: 14px
                        }

                        .int_width {
                            width: 80%;
                            margin: 0 auto
                        }
                    }
                    .pricebox {
                    width: 49%
                }

                @media only screen and (max-width: 767px) {
                    .pricebox {
                        width: 100%;
                    }
                }
                </style>

    
@endsection

@section('content')
<div id="headline">
            <h1>Business Hosting Packages</h1>
        </div>
        <div class="content-adj">
            <h2>Corporate Website Hosting in Saudi Arabia</h2>
            <p>
                Ovohost's Business Hosting Packages are designed for Corporate Websites, giving advanced stabilty,
                speed and 100% uptime!
                Your data is hosted on Raid10 SSD drives which result in upto 6X faster speed & data is secure even in
                the case of hard drive failure.
                <strong>Want your business website hosted? Ovohost is the reliable hosting provider in
                    Saudi Arabia!</strong>
            </p>

            

            <div id="packages">
                <div class="pricebox pricebox-a">
                    <div class="pricehead">Business Pro</div>
                    <div class="amount">SAR 2,000/month</div>
                    <ul>
                        <li><strong>20 GB SSD Disk</strong></li>
                        <li>100GB/month Bandwidth</li>
                        <li><strong>2 GB RAM, 2 CPU Core</strong></li>
                        <li class="red">
                            Free Domain Registration<br>
                            (.com/.net/.pk/.com.pk)
                        </li>
                        <li class="red">Free SSL Certificate</li>
                        <li><strong>Host 1 Domain</strong></li>
                        <li>Unlimited Emails, FTP & SubDomains</li>
                        <li>cPanel, PHP, MySQL</li>
                        <li>Realtime Malware Scanning</li>
                        <li>Premium Email Delivery</li>
                        <li>Off-site Backups</li>
                        <li>99.9% Uptime, 24x7 Helpline</li>
                    </ul>
                    <p style="text-align: center; margin-bottom: 10px">
                        <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=42" class="order-b">Order Now!</a>
                        <!-- <a href="account/carta5fb.html?a=add&amp;pid=42" class="order-b">Order Now!</a> -->
                    </p>
                </div>

                <div class="pricebox">
                    <div class="pricehead">Business Pro+</div>
                    <div class="amount">SAR 3,500/month</div>
                    <ul>
                        <li><strong>30 GB SSD Disk</strong></li>
                        <li>100GB/month Bandwidth</li>
                        <li><strong>3 GB RAM, 2 CPU Core</strong></li>
                        <li class="red">
                            Free Domain Registration<br>
                            (.com/.net/.pk/.com.pk)
                        </li>
                        <li class="red">Free SSL Certificate</li>
                        <li><strong>Host 10 Domains</strong></li>
                        <li>Unlimited Emails, FTP & SubDomains</li>
                        <li>cPanel, PHP, MySQL</li>
                        <li>Realtime Malware Scanning</li>
                        <li>Premium Email Delivery</li>
                        <li>Off-site Backups</li>
                        <li>99.9% Uptime, 24x7 Helpline</li>
                    </ul>
                    <p style="text-align: center; margin-bottom: 10px">
                        <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=32" class="order-b">Order Now!</a>
                        <!-- <a href="account/indexb494.html?a=add&amp;pid=32" class="order-b">Order Now!</a> -->
                    </p>
                </div>

            </div>

            <div class="clear"></div>


            <div id="features_new">
               


                <h3 style="text-align: center; margin-bottom: 30px; font-size: 20px;">Great features with all web
                    hosting
                    plans</h3>

                <style>
                 
                </style>

                <div id="features_block">

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/domains.png" alt="" width="80">
                            <p><strong>Free Domain Names</strong></p>
                            <p>We offer free domain name with web hosting plans</p>
                        </div>
                    </div>

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/site-security.png" alt="" width="80">
                            <p><strong>Free SSL Certificates</strong></p>
                            <p>We take site security serious! Offering free SSL with all plans</p>
                        </div>
                    </div>

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/uptime.png" alt="" width="80">
                            <p><strong>99.9% Uptime</strong></p>
                            <p>We guarantee 99.9% uptime on all web hosting plans</p>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/website-builder.png" alt="" width="80">
                            <p><strong>Website Builder</strong></p>
                            <p>Our hosting panel includes website builder so ou can start in no time</p>
                        </div>
                    </div>

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/e-commerce.png" alt="" width="80">
                            <p><strong>E-Commerce</strong></p>
                            <p>Our hosting plans supports Ecommerce websites!</p>
                        </div>
                    </div>

                    <div class="feature_box">
                        <div class="int_width">
                            <img src="images/ideas.png" alt="" width="80">
                            <p><strong>100+ Freebies</strong></p>
                            <p>You get 100+ one click applications and templates for free!</p>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>

            </div>


            <div class="block">


                <h3>Transfer your website / domain to us</h3>


                <p>


                    Getting your website transferred to ovohost is simple and easy, submit your transfer order and we
                    will take
                    care of the data.


                </p>


                <p>


                    As a transfer gift, you will get 10% cash back (discount voucher) once you have successfully moved
                    to us!
                    For further information please contact us.


                </p>


            </div>


            <div id="aboutus">


                <h3>About OvoHost</h3>


                <p>We offer web hosting and domain registration services in Saudi Arabia for individuals and businesses so
                    they can
                    setup their website in less than an hour. With out cPanel enabled hosting packages you can start
                    branding
                    your business on internet faster than ever before! Our hosting comes with hundreds of features
                    including a
                    rock solid 99.9% uptime, free domain name & 30 days money back guarantee - so you don't have to
                    worry about
                    your money! </p>


                <table width="80%" style="margin: 20px auto;">


                    <tr>


                        <td width="33%" style="text-align: center;"><img src="images/money-back-guarantee.gif"
                                alt="money back guarantee" height="140"></td>


                        <td width="33%" style="text-align: center"><img src="images/uptime-guarantee.png"
                                alt="uptime guarantee" height="140"></td>


                        <td width="33%" style="text-align: center"><img src="images/satisfaction-guarantee.png"
                                alt="satisfaction guarantee" height="140"></td>


                    </tr>


                    <tr>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">30 Days
                            Money Back
                            Guarantee - so you can try hassle free!
                        </td>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">You get
                            99.9% uptime
                            on our Saudi Arabia based servers!
                        </td>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">We go extra
                            mile for
                            customer's satisfaction whenever you need!
                        </td>


                    </tr>


                </table>


            </div>

            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
