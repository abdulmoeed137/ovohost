@extends('layouts.app')

@section('title', 'Premium SSD Web Hosting in Saudi Arabia | OVOHOST')

@section('content')
    <div id="headline">
        <h1>SSD Web Hosting <sup
                style='font-size: 12px; padding: 1px 5px; background-color:#2fb145; color: white; border-radius: 2px'>NEW</sup>
        </h1>
    </div>
    <div class="content-adj">
        <p>OvoHost's <strong>SSD Web Hosting offers Top Notch speed and reliabilty</strong> (upto 6X Faster than HDD
            hosting), SSD Hosting Plans are for everyone who needs Rock Solid Speed & Uptime.</p>

        <div id="packages">
            <div class="pricebox pricebox-a">
                <div class="pricehead">SSD I</div>
                <div class="amount">SAR 4,000/year</div>
                <ul>
                    <li><strong>2 GB SSD Disk</strong></li>
                    <li>25GB/month Bandwidth</li>
                    <li><strong>1 GB RAM, 1 CPU Core</strong></li>
                    <li class="red">Free SSL Certificate</li>
                    <li><strong>Host 1 Domain</strong></li>
                    <li>10 Emails, FTP & SubDomains</li>
                    <li>1 MySQL Database</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>Realtime Malware Scanning</li>
                    <li>Premium Email Delivery</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=37" class="order-b">Order Now!</a>
                    <!-- <a href="account/cart1b5f.html?a=add&amp;pid=37" class="order-b">Order Now!</a> -->
                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">SSD II</div>
                <div class="amount">SAR 6,000/year</div>
                <ul>
                    <li><strong>4 GB SSD Disk</strong></li>
                    <li>50GB/month Bandwidth</li>
                    <li><strong>1 GB RAM, 1 CPU Core</strong></li>
                    <li class="red">Free SSL Certificate</li>
                    <li><strong>Host 2 Domain</strong></li>
                    <li>20 Emails, FTP & SubDomains</li>
                    <li>2 MySQL Database</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>Realtime Malware Scanning</li>
                    <li>Premium Email Delivery</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=38" class="order-b">Order Now!</a>
                    <!-- <a href="account/cart0450.html?a=add&amp;pid=38" class="order-b">Order Now!</a> -->
                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">SSD III</div>
                <div class="amount">SAR 8,000/year</div>
                <ul>
                    <li><strong>6 GB SSD Disk</strong></li>
                    <li>75GB/month Bandwidth</li>
                    <li><strong>1 GB RAM, 1 CPU Core</strong></li>
                    <li class="red">Free SSL Certificate</li>
                    <li><strong>Host 2 Domain</strong></li>
                    <li>30 Emails, FTP & SubDomains</li>
                    <li>2 MySQL Database</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>Realtime Malware Scanning</li>
                    <li>Premium Email Delivery</li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=39" class="order-b">Order Now!</a>
                    <!-- <a href="account/cart3823.html?a=add&amp;pid=39" class="order-b">Order Now!</a> -->
                </p>
            </div>

            <div class="pricebox" style="border-color:#f4590b !important">
                <div class="pricehead" style="background:#e38d36 !important">SSD Max I</div>
                <div class="amount">SAR 10,000/year</div>
                <ul>
                    <li><strong>8 GB SSD Disk</strong></li>
                    <li>100GB/month Bandwidth</li>
                    <li><strong>2 GB RAM, 2 CPU Cores</strong></li>
                    <li class="red">Free .COM Domain Registration</li>
                    <li class="red">Free SSL Certificate</li>
                    <li><strong>Host 2 Domain</strong></li>
                    <li>50 Emails, FTP & SubDomains</li>
                    <li>2 MySQL Database</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>Realtime Malware Scanning</li>
                    <li>Premium Email Delivery</li>
                    <li><strong>Off-site Backups</strong></li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=40" class="order-b">Order Now!</a>
                    <!-- <a href="account/cart2bee.html?a=add&amp;pid=40" class="order-b">Order Now!</a> -->
                </p>
            </div>

            <div class="pricebox">
                <div class="pricehead">SSD Max II</div>
                <div class="amount">SAR 15,000/year</div>
                <ul>
                    <li><strong>15 GB SSD Disk</strong></li>
                    <li>100GB/month Bandwidth</li>
                    <li><strong>2 GB RAM, 2 CPU Cores</strong></li>
                    <li class="red">Free .COM Domain Registration</li>
                    <li class="red">Free SSL Certificate</li>
                    <li><strong>Host 5 Domain</strong></li>
                    <li>50 Emails, FTP & SubDomains</li>
                    <li>5 MySQL Database</li>
                    <li>cPanel, PHP, MySQL</li>
                    <li>Realtime Malware Scanning</li>
                    <li>Premium Email Delivery</li>
                    <li><strong>Off-site Backups</strong></li>
                    <li>99.9% Uptime, 24x7 Helpline</li>
                </ul>
                <p style="text-align: center; margin-bottom: 10px">
                    <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=41" class="order-b">Order Now!</a>
                    <!-- <a href="account/cartc5b4.html?a=add&amp;pid=41" class="order-b">Order Now!</a> -->
                </p>
            </div>

        </div>

        <div class="clear"></div>

        <section class="feature-block">
            <h3 style="text-align: center; margin-bottom: 15px; font-size: 20px">
                Why choose SSD Web Hosting plans:
            </h3>

            <p style="text-align:center;width:90%; margin:0px auto 20px auto">SSD Disks are upto 6 times faster than HDD -
                you get unparallel experience when you host on SSD plans and your site out performance the ones hosted on
                HDD disks! Other benefits with our SSD plans are as follows:</p>

            <div class="qtr-service">
                <h4>Raid Protection</h4>
                <p>We use array of Disk to replicate the data, real time. So your data is safe even if a disk crashes!</p>
            </div>

            <div class="qtr-service">
                <h4>Malware Scanning</h4>
                <p>We use trusted Third Party tool to scan uploaded data realtime - infected files are caught before they
                    destroy your site.</p>
            </div>

            <div class="qtr-service">
                <h4>Enterprise Servers</h4>
                <p>we use enterprise grade servers with SSD disks to host the sites! You get the best in class hosting
                    experience</p>
            </div>

            <div class="qtr-service">
                <h4>Email Delivery</h4>
                <p>We use delivery expert tools to scan for spam and deliver your emails to Inbox - no IP blacklisting! </p>
            </div>

            <div class="clear"></div>

            <h3
                style="text-align: center; margin-bottom: 15px; font-size: 20px; margin-top:10px; padding-top:10px; border-top:1px solid #b1b1b1">
                SSD v/s HDD Web Hosting Comparison:
            </h3>

            <p style="text-align:center">
                <img src="{{ asset('images/ssd-hosting-easyhostpk.png') }}" alt="Easyhost SSD Hosting">
            </p>

        </section>

        <div class="clear"></div>

        <div class="content-adj">
            <div id="faq">

                <h3 style="margin-bottom: 10px">Got questions regarding SSD Web Hosting?</h3>

                <p class="accordion">Who should choose SSD Hosting?</p>
                <div class="panel">
                    Anyone can go for SSD Hosting, SSD works better than HDD for all kinds of website. If you don't have
                    enough budget or you are beginner, you can go for regular web hosting plans.
                </div>

                <p class="accordion">Does SSD hosting support Wordpress?</p>
                <div class="panel">
                    Yes, SSD is recommended for Wordpress sites.
                </div>

                <p class="accordion">What is off-site backup?</p>
                <div class="panel">
                    We go one step ahead and take Off-site backups of accounts on a monthly basis (for applicable plans).
                    This way your data is secured on another location and can be retrieved in case of a diasater.
                </div>


            </div>
        </div>
        <div class="clear"></div>

        <p style="line-height: 30px; text-align: center">
            Need help? <a href="{{ route('contact') }}">Contact Us Now!</a>
        </p>

        <div class="clear"></div>

        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>
        <div class="clear"></div>
    </div>
@endsection

@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            var acc = document.getElementsByClassName("accordion");
            var panel = document.getElementsByClassName('panel');

            for (var i = 0; i < acc.length; i++) {
                acc[i].onclick = function() {
                    var setClasses = !this.classList.contains('active');
                    setClass(acc, 'active', 'remove');
                    setClass(panel, 'show', 'remove');

                    if (setClasses) {
                        this.classList.toggle("active");
                        this.nextElementSibling.classList.toggle("show");
                    }
                }
            }

            function setClass(els, className, fnName) {
                for (var i = 0; i < els.length; i++) {
                    els[i].classList[fnName](className);
                }
            }

        });
    </script>
@endsection
