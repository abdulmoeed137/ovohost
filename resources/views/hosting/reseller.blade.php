@extends('layouts.app')

@section('title', 'Reseller Hosting Saudi Arabia | cPanel & WHM Reseller | OvoHost')

@section('styles')
    <style>
        #features_new {
            width: 100%;
            margin-bottom: 20px;
            margin-top: 20px;
        }

        #features_new h3 {
            padding: 10px 40px
        }

        #features_new ul {
            margin-left: 70px;
            margin-right: 50px
        }

        #features_new ul li {
            float: left;
            width: 25%;
            list-style-image: url('images/ricon.png');
            line-height: 40px;
        }

        #features_block {
            margin: 0 auto;
            width: 90%
        }

        .feature_box {
            width: 33.3%;
            text-align: center;
            float: left;
            margin-bottom: 30px
        }

        .feature_box p {
            font-size: 14px
        }

        .int_width {
            width: 240px;
            margin: 0 auto;
        }

        @media only screen and (max-width: 767px) {
            #features_block {
                margin: 0 auto;
                width: 100%
            }

            .feature_box {
                width: 100%;
                text-align: center;
                float: none;
                margin-bottom: 30px
            }

            .feature_box p {
                font-size: 14px
            }

            .int_width {
                width: 80%;
                margin: 0 auto
            }
        }

    </style>
@endsection

@section('content')
<div id="headline">
        <h1>Reseller Hosting Saudi Arabia</h1>
    </div>
    <div class="content-adj">
            <h2>Reseller Hosting Packages | Ovohost Saudi Arabia</h2>
            <p>
                All reseller hosting plans include free domain name, site builder, wordpress, ecommerce support and much
                more.
            </p>
            <div id="table">
                <table width="100%" cellpadding="0" cellspacing="0" id="plansblock"
                    style="margin-top: 20px; margin-bottom:20px">
                    <thead>
                        <td width="25%" class="lalign">Plans</td>
                        <td width="15%">RI</td>
                        <td width="15%">RII</td>
                        <td width="15%">RIII</td>
                        <td width="15%">RIV</td>
                        <td width="15%">RV</td>
                    </thead>
                    <tr>
                        <td width="25%" class="lalign">Disk Space</td>
                        <td width="15%">5 GB</td>
                        <td width="15%">15 GB</td>
                        <td width="15%">25 GB</td>
                        <td width="15%">35 GB</td>
                        <td width="15%">50 GB</td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Bandwidth</td>
                        <td width="15%">100 GB</td>
                        <td width="15%">200 GB</td>
                        <td width="15%">300 GB</td>
                        <td width="15%">400 GB</td>
                        <td width="15%">Unlimited</td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Free Domain</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">cPanel + WHM</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Host Domains</td>
                        <td width="15%">Unlimited</td>
                        <td width="15%">Unlimited</td>
                        <td width="15%">Unlimited</td>
                        <td width="15%">Unlimited</td>
                        <td width="15%">Unlimited</td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">cPanel Accounts</td>
                        <td width="15%">20</td>
                        <td width="15%">30</td>
                        <td width="15%">40</td>
                        <td width="15%">60</td>
                        <td width="15%">80</td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign"><strong>Monthly Fee</strong></td>
                        <td width="15%"><strong>-</strong></td>
                        <td width="15%"><strong>-</strong></td>
                        <td width="15%"><strong>SR.1,200/m</strong></td>
                        <td width="15%"><strong>SR.1,500/m</strong></td>
                        <td width="15%"><strong>SR.1,800/m</strong></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign"><strong>Annual Fee</strong></td>
                        <td width="15%"><strong>SR.6,000/yr</strong></td>
                        <td width="15%"><strong>SR.8,000/yr</strong></td>
                        <td width="15%"><strong>SR.10,000/yr</strong></td>
                        <td width="15%"><strong>SR.13,000/yr</strong></td>
                        <td width="15%"><strong>SR.16,000/yr</strong></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign"></td>
                        <td width="15%">
                            <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=6" class="order-btn">Order</a>
                            <!-- <a href="account/cart0eac.html?a=add&amp;pid=6" class="order-btn">Order</a> -->
                        </td>
                        <td width="15%">
                            <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=7" class="order-btn">Order</a>
                            <!-- <a href="account/cartb837.html?a=add&amp;pid=7" class="order-btn">Order</a> -->
                        </td>
                        <td width="15%">
                            <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=8" class="order-btn">Order</a>
                            <!-- <a href="account/cart9af7.html?a=add&amp;pid=8" class="order-btn">Order</a> -->
                        </td>
                        <td width="15%">
                            <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=9" class="order-btn">Order</a>
                            <!-- <a href="account/cart49df.html?a=add&amp;pid=9" class="order-btn">Order</a> -->
                        </td>
                        <td width="15%">
                            <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=10" class="order-btn">Order</a>
                            <!-- <a href="account/cart3c4c.html?a=add&amp;pid=10" class="order-btn">Order</a> -->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"><strong>Other Resller Hosting Features</strong></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Datacenter</td>
                        <td width="15%">Saudi Arabia</td>
                        <td width="15%">Saudi Arabia</td>
                        <td width="15%">Saudi Arabia</td>
                        <td width="15%">Saudi Arabia</td>
                        <td width="15%">Saudi Arabia</td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">24/7 Support</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">99.9% Uptime</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">MoneyBack Guarantee</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">WordPress Hosting</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">phpMyAdmin</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Softaculous &#8211; <a
                                href="http://demo.softaculous.com/enduser/">Demo</a></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="25%" class="lalign">Audio Streaming</td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                        <td width="15%"><img src="images/ricon.png" alt="" /></td>
                    </tr>
                </table>
            </div>
            <div id="features" style="margin-top: 20px">
                <h3>Reseller Hosting Includes</h3>
                <ul>
                    <li>Free domain registration</li>
                    <li>WHM + cPanel control panel</li>
                    <li>99.99% uptime delivery</li>
                    <li>30 days money back guarantee</li>
                    <li>24x7 sms, whatsapp & email support</li>
                </ul>
                <div class="clear"></div>
                <p style="margin: 5px 0px"><strong>and...</strong></p>
                <ul>
                    <li>PHP, MySQL, PHP myAdmin</li>
                    <li>Personalized email accounts</li>
                    <li>Self backup functions</li>
                    <li>FTP user accounts</li>
                    <li>File manager</li>
                    <li>Password protected directories</li>
                    <li>Detailed website statistics</li>
                    <li>Wordpress supported</li>
                    <li>Magento supported</li>
                    <li>Ecommerce ready</li>
                    <li>Mailing lists</li>
                    <li>1 click scripts installer</li>
                    <li>Free site builder</li>
                    <li>Auto backup utility</li>
                    <li>100+ powerful scripts pack</li>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="block">
                <h3>Transfer your website / domain to us</h3>
                <p>
                    Getting your website transferred to ovohost is simple and easy, submit your transfer order and we
                    will take
                    care of the data.
                </p>
                <p>
                    As a transfer gift, you will get 10% cash back (discount voucher) once you have successfully moved
                    to us!
                    For further information please contact us.
                </p>
            </div>
            <div id="aboutus">
                <h3>About OvoHost</h3>
                <p>We offer web hosting and domain registration services in Saudi Arabia for individuals and businesses so
                    they can
                    setup their website in less than an hour. With out cPanel enabled hosting packages you can start
                    branding
                    your business on internet faster than ever before! Our hosting comes with hundreds of features
                    including a
                    rock solid 99.9% uptime, free domain name & 30 days money back guarantee - so you don't have to
                    worry about
                    your money! </p>
                <table width="80%" style="margin: 20px auto;">
                    <tr>
                        <td width="33%" style="text-align: center;"><img src="images/money-back-guarantee.gif"
                                alt="money back guarantee" height="140"></td>
                        <td width="33%" style="text-align: center"><img src="images/uptime-guarantee.png"
                                alt="uptime guarantee" height="140"></td>
                        <td width="33%" style="text-align: center"><img src="images/satisfaction-guarantee.png"
                                alt="satisfaction guarantee" height="140"></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">30 Days
                            Money Back
                            Guarantee - so you can try hassle free!
                        </td>
                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">You get
                            99.9% uptime
                            on our Saudi Arabia based servers!
                        </td>
                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">We go extra
                            mile for
                            customer's satisfaction whenever you need!
                        </td>
                    </tr>
                </table>
            </div>
            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
