@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/all.min6892.css?v=db9674') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style-whmcs.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #ffffff;
        }

        .row:first {
            margin-top: 2rem;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 main-content">
            <div class="logincontainer with-social">

                <div class="header-lined">
                    <h1>Support <small>This page is restricted</small></h1>
                </div>
                <div class="providerLinkingFeedback"></div>

                <div class="row">
                    <div class="col-sm-7">

                        <form method="post" action="https://www.easyhost.pk/account/index.php?rp=/login"
                            class="login-form" role="form">
                            <input type="hidden" name="token" value="9cbdad5cdb5248a300bd8e2e56822d409fca3b68" />
                            <div class="form-group">
                                <label for="inputEmail">Email Address</label>
                                <input type="email" name="username" class="form-control" id="inputEmail"
                                    placeholder="Enter email" autofocus>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" name="password" class="form-control" id="inputPassword"
                                    placeholder="Password" autocomplete="off">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="rememberme" /> Remember Me
                                </label>
                            </div>
                            <div class="text-center margin-bottom">
                                <div class="row">
                                </div>
                            </div>
                            <div align="center">
                                <input id="login" type="submit"
                                    class="btn btn-primary btn-recaptcha btn-recaptcha-invisible" value="Login" /> <a
                                    href="index0839.html?rp=/password/reset" class="btn btn-default">Forgot Password?</a>
                            </div>
                        </form>

                    </div>
                    <div class="col-sm-5">

                        <div id="providerLinkingMessages" class="hidden">
                            <p class="providerLinkingMsg-preLink-init_failed">
                                <span class="provider-name"></span> is unavailable at this time. Please try again later.
                            </p>
                            <p class="providerLinkingMsg-preLink-connect_error">
                                <strong>Error</strong> We were unable to connect your account. Please contact your system
                                administrator.
                            </p>
                            <p class="providerLinkingMsg-preLink-complete_sign_in">
                                Please complete sign in with your chosen service provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-2fa_needed">
                                Automatic sign-in successful! Redirecting...
                            </p>
                            <p class="providerLinkingMsg-preLink-linking_complete">
                                <strong>Success!</strong> Your account is now linked with your :displayName account.
                            </p>
                            <p class="providerLinkingMsg-preLink-login_to_link-signin-required">
                                <strong>Link Initiated!</strong> Please complete sign in to associate this service with your
                                existing account. You will only have to do this once.
                            </p>
                            <p class="providerLinkingMsg-preLink-login_to_link-registration-required">
                                <strong>Link Initiated!</strong> Please complete the registration form below.
                            </p>
                            <p class="providerLinkingMsg-preLink-checkout-new">
                                <strong>Link Initiated!</strong> Please complete your new account information.
                            </p>
                            <p class="providerLinkingMsg-preLink-other_user_exists">
                                <strong>Error</strong> This account is already connected to an existing account with us.
                                Please choose a different account at the third party authentication provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-already_linked">
                                <strong>Error</strong> This account is already connected to your account with us. Please
                                choose a different account at the third party authentication provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-default">
                                <strong>Error</strong> We were unable to connect your account. Please contact your system
                                administrator.
                            </p>
                        </div>


                        <div class="providerPreLinking" data-link-context="login" data-hide-on-prelink=0
                            data-disable-on-prelink=0>
                            <div class="social-signin-btns">
                                <button class="btn btn-social btn-facebook fb-login-button" data-max-rows="1"
                                    data-size="medium" data-button-type="login_with" data-show-faces="false"
                                    data-auto-logout-link="false" data-use-continue-as="false"
                                    data-scope="public_profile,email" onclick="onLoginClick()" type="button">
                                    <i class="fab fa-facebook"></i>
                                    Sign in with Facebook
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/scripts.min6892.js?v=db9674') }}"></script>
    <script type="text/javascript" src="{{ asset('js/StatesDropdown.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/PasswordStrength.js') }}"></script>
    <script>
        window.onerror = function(e) {
            WHMCS.authn.provider.displayError();
        };

        window.fbAsyncInit = function() {
            FB.init({
                appId: "243984456286963",
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true, // parse social plugins on this page
                version: "v2.8" // use graph api version 2.8
            });
        };

        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "../../connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, "script", "facebook-jssdk"));

        function onLoginClick() {
            WHMCS.authn.provider.preLinkInit();

            FB.login(
                function(response) {
                    var feedbackContainer = jQuery(".providerLinkingFeedback");
                    var btnContainer = jQuery(".providerPreLinking");

                    var failIfExists = 0;
                    if ("login" === "register" ||
                        "login" === "connect"
                    ) {
                        failIfExists = 1;
                    }

                    var context = {
                        htmlTarget: "login",
                        targetLogin: "login",
                        targetRegister: "register",
                        redirectUrl: "%2Faccount%2Fclientarea.php"
                    };

                    if (response.status === 'connected') {
                        var config = {
                            url: "/account/index.php?rp=/auth/provider/facebook_signin/finalize",
                            method: "POST",
                            dataType: "json",
                            data: {
                                accessToken: response.authResponse.accessToken,
                                fail_if_exists: failIfExists,
                                token: "9cbdad5cdb5248a300bd8e2e56822d409fca3b68",
                                cartCheckout: 0
                            }
                        };
                        var provider = {
                            name: "Facebook",
                            icon: "<i class=\"fab fa-facebook\"></i> "
                        };

                        var providerDone = function() {
                            FB = null;
                        };
                        var providerError = function() {};
                    } else if (!response.status) {
                        response.status = "unknown";
                    }

                    switch (response.status) {
                        case "connected":
                            WHMCS.authn.provider.signIn(config, context, provider, providerDone, providerError);
                            break;
                        case "not_authorized":
                            feedbackContainer.text(
                                'You did not authorize the use of Facebook for authentication. We can\'t use it to log you in.'
                            ).slideDown();
                            break;
                        case "unknown":
                            feedbackContainer.slideUp();
                    }
                });
        }
    </script>
@endsection
