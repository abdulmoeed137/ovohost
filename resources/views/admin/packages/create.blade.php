@extends('admin.packages.form')

@section('for', 'Create')
@section('link', route('admin.packages.store'))

@section('form')
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" class="form-control" required>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="custom-select" required>
                    <option value="1">Linux</option>
                    <option value="2">Windows</option>
                </select>
            </div>
        </div>
        <div class="col-sm-3 align-self-center">
            <div class="form-group">
                <div class="custom-control custom-checkbox mt-4">
                    <input type="checkbox" class="custom-control-input" id="premium" name="premium">
                    <label class="custom-control-label" for="premium">Premium</label>
                </div>
            </div>
        </div>
        <div class="col-sm-3 align-self-center">
            <div class="form-group">
                <div class="custom-control custom-checkbox mt-4">
                    <input type="checkbox" class="custom-control-input" id="best" name="best">
                    <label class="custom-control-label" for="best">Best Selling</label>
                </div>
            </div>
        </div>
        {{-- <div class="col-sm-3">
            <div class="form-group">
                <label for="position">Position</label>
                <select name="position" id="position" class="custom-select" required>
                    <option value="0">Last</option>
                    @for ($i = 1; $i <= $positions['1']; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
        </div> --}}
        <div class="col-sm-12">
            <hr>
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h5 class="mb-0">Duration</h5>
                <button type="button" class="btn btn-sm btn-info" onclick="addEl('durations')">Add</button>
            </div>
            <div id="durations">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="no-1">Period Duration</label>
                            <input type="number" id="no-1" name="no[]" class="form-control" step="any" min="1" required>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="period-1">Period</label>
                            <select class="custom-select" id="period-1" name="period[]">
                                <option value="1">Days</option>
                                <option value="2">Weeks</option>
                                <option value="3">Months</option>
                                <option value="4">Years</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price-1">Price</label>
                            <input type="number" id="price-1" name="price[]" class="form-control" step="any" min="0"
                                required>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="save-1">Saving</label>
                            <input type="number" id="save-1" name="save[]" class="form-control" step="any" min="0"
                                required>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="link-1">Link</label>
                            <input type="url" id="link-1" name="link[]" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-2 align-self-center">
                        <div class="custom-control custom-checkbox mt-2">
                            <input type="checkbox" class="custom-control-input" id="domain-1" name="domain[]">
                            <label class="custom-control-label" for="domain-1">Free Domain</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h5 class="mb-0">Features</h5>
                <button type="button" class="btn btn-sm btn-info" onclick="addEl('features')">Add</button>
            </div>
            <div id="features">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="text-1">Feature</label>
                            <input type="text" id="text-1" name="text[]" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="class-1">Class (optional)</label>
                            <input type="text" id="class-1" name="class[]" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
