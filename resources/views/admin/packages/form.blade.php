@extends('admin.app')

@section('styles')
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">@yield('for') Package</h4>
            <form method="POST" class="mt-3">
                @csrf
                @yield('form')
                <button type="submit" id="submit" class="btn btn-primary mx-auto">@yield('for')</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const positions = {!! json_encode($positions) !!}
        document.addEventListener("submit", function(e) {
            e.preventDefault()
            packageData(e.target)
        })

        // document.querySelector("#category").addEventListener("change", function(e) {
        //     const pEl = document.getElementById("position")
        //     let opts = ""
        //     if (!pEl.dataset.hasOwnProperty("pos") || positions[e.target.value] == 0) {
        //         opts = "<option value='0'>Last</option>"
        //     }
        //     pEl.options.length = 0
        //     for (let i = 1; i <= positions[e.target.value]; i++) {
        //         opts += `<option value='${i}'>${i}</option>`
        //     }
        //     pEl.innerHTML = opts
        //     if (pEl.dataset.hasOwnProperty("pos") &&
        //         pEl.dataset.pos.split("-")[0] == e.target.value) {
        //         pEl.value = pEl.dataset.pos.split("-")[1]
        //     }
        // })

        function addEl(p) {
            const el = document.getElementById(p)
            const rowEl = el.lastElementChild.cloneNode(true)
            const len = el.children.length
            const chEls = rowEl.querySelectorAll("input")
            for (let i = 0; i < chEls.length; i++) {
                const id = `${chEls[i].id.split("-")[0]}-${+len+1}`
                if (i != 1) {
                    if (i != 3) {
                        chEls[i].value = ""
                        chEls[i].previousElementSibling.setAttribute("for", id)
                    } else {
                        chEls[i].checked = false
                        chEls[i].nextElementSibling.setAttribute("for", id)
                    }
                    chEls[i].id = id
                }
            }
            if (len == 1) {
                rowEl.append(removeEl(p == "durations" ? 2 : 3))
            }
            el.append(rowEl)
        }

        function removeEl(size) {
            const divEl = document.createElement("div")
            divEl.setAttribute("class", `col-sm-${size} align-self-center`)
            divEl.innerHTML =
                "<button type='button' class='btn btn-sm btn-danger mt-1' onclick='removeRow(event)'><i class='fas fa-times x-color'></i></button>"
            return divEl
        }

        function removeRow(e) {
            const el = e.target.tagName == "I" ? e.target.parentElement.parentElement : e.target.parentElement
            el.parentElement.remove()
        }

        async function packageData(el) {
            const formData = new FormData(el)
            formData.delete("domain[]")
            document.querySelectorAll("input[name='domain[]']").forEach(el => {
                formData.append("domain[]", el.checked ? 1 : 0)
            })
            removeErrors()
            const btn = document.getElementById("submit")
            btn.disabled = true
            await axios.post("@yield('link')", formData)
                .then(res => {
                    if (res.data.success) {
                        alert(res.data.message, location.href = "{{ route('admin.packages.index') }}")
                    }
                })
                .catch(error => {
                    if (error.response.data.errors) {
                        setErrors(error.response.data.errors)
                    } else {
                        console.log(error);
                    }
                }).finally(() => {
                    btn.disabled = false
                })
        }

        function setErrors(errors, pre = null) {
            let inputEl = null;
            for (const [key, elErrors] of Object.entries(errors)) {
                let elName = key.split(".")[0]
                if (pre != null) {
                    elName = pre + elName
                }
                if (key.indexOf(".") >= 0) {
                    elName += `-${++key.split(".")[1]}`
                }
                inputEl = document.querySelector(`[id="${elName}"]`)
                if (!!inputEl) {
                    inputEl.classList.add("is-invalid")
                    for (const error of elErrors) {
                        inputEl.insertAdjacentHTML("afterend",
                            `<div class="invalid-feedback">${error}</div>`
                        )
                    }
                }
            }
        }

        function removeErrors() {
            const classEls = document.querySelectorAll(".is-invalid")
            const errorEls = document.querySelectorAll(".invalid-feedback")
            if (classEls != null || classEls.length > 0) {
                for (const classEl of classEls) {
                    classEl.classList.remove("is-invalid")
                }
            }
            if (errorEls != null || errorEls.length > 0) {
                for (const errorEl of errorEls) {
                    errorEl.remove()
                }
            }
        }
    </script>
@endsection
