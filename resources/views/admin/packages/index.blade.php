@extends('admin.app')

@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css"
        crossorigin="anonymous">
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center mb-4">
                <h4 class="mb-0">Packages</h4>
                <a href="{{ route('admin.packages.create') }}" class="btn btn-danger">Create</a>
            </div>
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered w-100">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Position</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($packages as $key => $package)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $package->title }}</td>
                                <td>{{ $package->category_id == 1 ? 'Linux' : 'Windows' }}</td>
                                <td>1</td>
                                <td>{{ $package->created_at->format('d-m-Y') }}</td>
                                <td>
                                    <a class="btn btn-primary"
                                        href="{{ route('admin.packages.edit', $package->id) }}">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
@endsection
