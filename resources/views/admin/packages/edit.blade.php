@extends('admin.packages.form')

@section('for', 'Edit')
@section('link', route('admin.packages.update', $package->id))

@section('form')
    @method('PUT')
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ $package->title }}" required>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="custom-select" required>
                    <option value="1" {{ $package->category_id == 1 ? 'selected' : '' }}>Linux</option>
                    <option value="2" {{ $package->category_id == 2 ? 'selected' : '' }}>Windows</option>
                </select>
            </div>
        </div>
        <div class="col-sm-3 align-self-center">
            <div class="form-group">
                <div class="custom-control custom-checkbox mt-4">
                    <input type="checkbox" class="custom-control-input" id="premium" name="premium"
                        {{ $package->is_premium == 1 ? 'checked' : '' }}>
                    <label class="custom-control-label" for="premium">Premium</label>
                </div>
            </div>
        </div>
        <div class="col-sm-3 align-self-center">
            <div class="form-group">
                <div class="custom-control custom-checkbox mt-4">
                    <input type="checkbox" class="custom-control-input" id="best" name="best"
                        {{ $package->is_best == 1 ? 'checked' : '' }}>
                    <label class="custom-control-label" for="best">Best Selling</label>
                </div>
            </div>
        </div>
        {{-- <div class="col-sm-3">
            <div class="form-group">
                <label for="position">Position</label>
                <select name="position" id="position" class="custom-select"
                    data-pos="{{ $package->category_id . '-' . $package->position }}" required>
                    @for ($i = 1; $i <= $positions['1']; $i++)
                        <option value="{{ $i }}" {{ $package->position == $i ? 'selected' : '' }}>
                            {{ $i }}
                        </option>
                    @endfor
                </select>
            </div>
        </div> --}}
        <div class="col-sm-12">
            <hr>
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h5 class="mb-0">Duration</h5>
                <button type="button" class="btn btn-sm btn-info" onclick="addEl('durations')">Add</button>
            </div>
            <div id="durations">
                @foreach ($package->durations as $key => $duration)
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="no-1">Period Duration</label>
                                <input type="number" id="no-1" name="no[]" class="form-control" step="any" min="1"
                                    value="{{ $duration->no }}" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="period-1">Period</label>
                                <select class="custom-select" id="period-1" name="period[]">
                                    <option value="1" {{ $duration->period == 1 ? 'selected' : '' }}>Days</option>
                                    <option value="2" {{ $duration->period == 2 ? 'selected' : '' }}>Weeks</option>
                                    <option value="3" {{ $duration->period == 3 ? 'selected' : '' }}>Months</option>
                                    <option value="4" {{ $duration->period == 4 ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="price-{{ $key + 1 }}">Price</label>
                                <input type="number" id="price-{{ $key + 1 }}" name="price[]" class="form-control"
                                    step="any" min="0" value="{{ $duration->price }}" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="save-{{ $key + 1 }}">Saving</label>
                                <input type="number" id="save-{{ $key + 1 }}" name="save[]" class="form-control"
                                    step="any" min="0" value="{{ $duration->save }}" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="link-{{ $key + 1 }}">Link</label>
                                <input type="url" id="link-{{ $key + 1 }}" name="link[]" class="form-control"
                                    value="{{ $duration->link }}" required>
                            </div>
                        </div>
                        <div class="col-sm-2 align-self-center">
                            <div class="custom-control custom-checkbox mt-2">
                                <input type="checkbox" class="custom-control-input" id="domain-{{ $key + 1 }}"
                                    name="domain[]" {{ $duration->domain == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="domain-{{ $key + 1 }}">Free Domain</label>
                            </div>
                        </div>
                        @if ($key > 0)
                            <div class="col-sm-2 align-self-center">
                                <button type='button' class='btn btn-sm btn-danger mt-1' onclick='removeRow(event)'><i
                                        class='fas fa-times x-color'></i></button>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h5 class="mb-0">Features</h5>
                <button type="button" class="btn btn-sm btn-info" onclick="addEl('features')">Add</button>
            </div>
            <div id="features">
                @foreach ($package->features as $key => $feature)
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="text-{{ $key + 1 }}">Feature</label>
                                <input type="text" id="text-{{ $key + 1 }}" name="text[]" class="form-control"
                                    value="{{ $feature->text }}" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="class-{{ $key + 1 }}">Class (optional)</label>
                                <input type="text" id="class-{{ $key + 1 }}" name="class[]" class="form-control"
                                    value="{{ $feature->class }}">
                            </div>
                        </div>
                        @if ($key > 0)
                            <div class="col-sm-2 align-self-center">
                                <button type='button' class='btn btn-sm btn-danger mt-1' onclick='removeRow(event)'><i
                                        class='fas fa-times x-color'></i></button>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
