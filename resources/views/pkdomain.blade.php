@extends('layouts.app')

@section('title', '.COM Domain Registration in Saudi Arabia | OvoHost')

@section('styles')

<style>

#pkdomain {
                        text-align: center;
                        font-size: 14px;
                        border-right: 1px solid #e1e1e1
                    }

                    #pkdomain tr td {
                        background: #f9f9f9;
                        padding: 6px;
                        border-bottom: 1px solid #e1e1e1;
                        border-left: 1px solid #e1e1e1
                    }

                    #pkdomain thead td {
                        background-color: #0e5077;
                        color: #fff;
                        padding: 10px;
                        font-weight: bold;
                    }

                    @media only screen and (max-width : 767px) {
                        #pkdomain {
                            text-align: center;
                            font-size: 12px
                        }

                        #pkdomain tr td {
                            background: #f9f9f9;
                            padding: 5px
                        }

                        #pkdomain thead td {
                            background-color: #0e5077;
                            color: #fff;
                            padding: 5px;
                            font-weight: bold;
                        }
                    }

                #leftpay {
                    float: left;
                    width: 49%
                }

                #rightpay {
                    float: right;
                    width: 49%
                }

                @media only screen and (max-width: 767px) {
                    #leftpay {
                        float: none;
                        width: 100%
                    }

                    #rightpay {
                        float: none;
                        width: 100%;
                        margin-top: 20px
                    }
                }

            
                    .do-checker {
                        background-color: #f9f9f9;
                        margin-bottom: 10px
                    }

                    .search_div input {
                        padding: 5px;
                        font-size: 20px;
                    }

                    .submit_div input {
                        padding: 5px 30px;
                        font-size: 20px;
                        color: #0c3754;
                        font-weight: bold;
                        margin-top: 10px
                    }

                    .select_div select {
                        padding: 5px;
                        font-size: 20px;
                        margin-top: 10px
                    }

                    
                .domainsbox {
                    width: 21%;
                    float: left;
                    background-color: #f9f9f9;
                    margin-left: 2%;
                    margin-right: 2%;
                    margin-top: 10px;
                    margin-bottom: 20px
                }

                .domainscircle {
                    margin: 10px auto;
                    border-radius: 50%;
                    background: #0e5077;
                    width: 150px;
                    height: 150px;
                    text-align: center;
                    color: white;
                    font-weight: bold;
                }

                .domainscircle h3 {
                    padding-top: 50px;
                    font-size: 22px
                }

                .domainscircle p {
                    font-size: 15px !important;
                }

                .domainsbox p {
                    text-align: center;
                    padding: 0px 10px;
                    font-size: 13px;
                }

                .lfamous {
                    padding: 5px;
                    background-color: darkorange;
                    font-weight: bold;
                    color: white
                }

                .searchref {
                    padding: 5px;
                    background-color: #0e5077;
                    color: white;
                    text-decoration: none
                }

                @media only screen and (max-width : 767px) {
                    .domainsbox {
                        width: 100%
                    }
                }
            
                
            </style>
@endsection

@section('content')

<div id="headline">
            <h1>.COM Domain Registration in Saudi Arabia</h1>
        </div>

        <div class="content-adj">
            

            <p style="margin-bottom: 10px">
            OvoHost is proud to offer cheap .COM domain name registration service in Saudi Arabia, you can buy .COM
                domains of your choice at low cost!
                Registering .COM domain is easy and affordable with us, just submit your order in less than 5 minuntes.
            </p>

            <div id="leftpay">

              

                <table width="100%" cellpadding="0" cellspacing="0" id="pkdomain" style="margin-top:0px">
                    <thead>
                        <td width="40%" class="lalign">.COM Extension</td>
                        <td width="30%">Registration</td>
                        <td width="30%">Renewal</td>
                    </thead>
                    <tr>
                        <td class="lalign">.COM</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.COM.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.ORG.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.NET.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.EDU.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.BIZ.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.WEB.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                    <tr>
                        <td class="lalign">.GOV.PK</td>
                        <td>SR.2350 / 2 years</td>
                        <td>SR.2350 / 2 years</td>
                    </tr>
                </table>
            </div>

            <div id="rightpay" style="background: #f9f9f9; text-align: center; border: 1px solid #e1e1e1">

                <h3 style="line-height: 40px; background-color: #0e5077; color: white" id="searchCOM">Search .COM Domain
                </h3>

                <p style="margin: 20px 0; font-weight: bold">Search .COM Domain for your Personal or Company Website:</p>

              

                <div class='do-checker'>
                    <form method="post" action='https://www.easyhost.pk/account/cart.php?a=add&amp;domain=register'>
                        <div class=" search_div">
                            <input required='required' title='Please fill out this field' type="search" name="sld"
                                id="search_domain" placeholder="Search .pk domains" value="" />
                        </div>
                        <div class="select_div">
                            <select name='tld'>
                                <option>.pk</option>
                                <option>.com.pk</option>
                                <option>.net.pk</option>
                                <option>.org.pk</option>
                                <option>.biz.pk</option>
                                <option>.edu.pk</option>
                                <option>.gov.pk</option>
                                <option>.web.pk</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="submit_div"><input type="submit" value="Search Domain"></div>
                    </form>
                    <div class="clear"></div>
                </div>

                <p style="margin-bottom: 10px"><i>Got questions? <a href="{{route('contact')}}">Contact Us Now!</a></i></p>

            </div>

            <div class="clear"></div>

          

            <h3 style="text-align: center; font-size: 25px; margin-top: 30px; margin-bottom: 20px">.com Extension General
                Use Guide</h3>

            <div class="domainsbox">
                <p class="lfamous">Most Used Extension!</p>
                <div class="domainscircle">
                    <h3>.COM</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">Most Used Extension!</p>
                <div class="domainscircle">
                    <h3>.COM.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">Best for Network & ISPs</p>
                <div class="domainscircle">
                    <h3>.NET.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Networking bodies & ISPs.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">Best for Organizations / NGOs</p>
                <div class="domainscircle">
                    <h3>.ORG.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Organizations & Not-for-Profit cause.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="clear"></div>

            <div class="domainsbox">
                <p class="lfamous">General / Business Use</p>
                <div class="domainscircle">
                    <h3>.BIZ.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">General / Business Use</p>
                <div class="domainscircle">
                    <h3>.WEB.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">For Edu. Institutes</p>
                <div class="domainscircle">
                    <h3>.EDU.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>For registered institutes to register.<br>Registered academies / schools and education centers use.
                </p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="domainsbox">
                <p class="lfamous">For Gov. Organizations</p>
                <div class="domainscircle">
                    <h3>.GOV.PK</h3>
                    <p>SR.2,350/2yr</p>
                </div>
                <p>For Registerd GOV. Org. to register.<br>Government departments, organizations can register.</p>
                <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a>
                </p>
            </div>

            <div class="clear"></div>

            <div class="content-adj">
                <div id="faq">

                    <h3 style="margin-bottom: 10px">Got questions regarding .COM Domain?</h3>

                    <p class="accordion">How much time is required for .COM domain registration?</p>
                    <div class="panel">
                        <p>You can submit .COM domain registration online in just couple minutes, search available COM
                            domain extension of your choice and complete your order. Once you pay your invoice, .COM
                            domain is registered in within 15-20 minutes.</p>
                    </div>

                    <p class="accordion">Where can i buy .COM Domain?</p>
                    <div class="panel">
                        <p>OvoHost is the best COM domains registration company Karachi, you can register .COM domains by
                            simply submitting your order on our website from anywhere in Saudi Arabia - search .COM domain
                            availability now!</p>
                    </div>

                    <p class="accordion">What if i need .COM domain with Hosting?</p>
                    <div class="panel">
                        <p>You can take a look at out <a href="pk-domain-hosting.html">COM Domain Hosting Packages</a>
                            that include .COM Domain name registration at an affordable price!</p>
                    </div>


                </div>

               
            </div>

            <div class="clear"></div>

            <p style="line-height: 30px; text-align: center">
                Need help? <a href="{{route('contact')}}">Contact Us Now!</a>
            </p>


            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection

@section('styles')
<script>
                    document.addEventListener("DOMContentLoaded", function (event) {


                        var acc = document.getElementsByClassName("accordion");
                        var panel = document.getElementsByClassName('panel');

                        for (var i = 0; i < acc.length; i++) {
                            acc[i].onclick = function () {
                                var setClasses = !this.classList.contains('active');
                                setClass(acc, 'active', 'remove');
                                setClass(panel, 'show', 'remove');

                                if (setClasses) {
                                    this.classList.toggle("active");
                                    this.nextElementSibling.classList.toggle("show");
                                }
                            }
                        }

                        function setClass(els, className, fnName) {
                            for (var i = 0; i < els.length; i++) {
                                els[i].classList[fnName](className);
                            }
                        }

                    });
                </script>
@endsection
