@extends('layouts.app')

@section('title', ".COM Domain Whois | Find domain's Expiry & Ownership details")

@section('styles')
    <style>
                pre {
                    white-space: pre-wrap;
                    /* css-3 */
                    white-space: -moz-pre-wrap;
                    /* Mozilla, since 1999 */
                    white-space: -pre-wrap;
                    /* Opera 4-6 */
                    white-space: -o-pre-wrap;
                    /* Opera 7 */
                    word-wrap: break-word;
                    /* Internet Explorer 5.5+ */
                }
    </style>
@endsection

@section('content')

<div id="headline">
<h1>PK Domain Whois</h1>
        </div>

        <div class="content-adj">
            <p style="margin-bottom:10px; font-size:14px">Check WHOIS (Registration date, Expiry date, Registrant
                details & Nameservers) & Traffic stats of any .COM, .COM, .NET, .ORG, .COM.PK or other domain extensions.
                This tool will allow you to find the registration details of domains and can find the hosting assigned
                to it.</p>


           

            <form action="https://www.easyhost.pk/whois-submit.php" method="post" style="margin-bottom: 20px">

                Domain name:

                <input type="text" name="domain" id="domain" value="" style="padding:5px">

                <input type="submit" value="Lookup" style="padding:5px"></p>

            </form>



            <div style="font-size:14px;">


            </div><br><br><br><br><br><br><br>
            <p style='font-size:12px'><strong>Disclaimer:</strong> Disclaimer: Please note we do not have any
                affiliation or link with PKNIC or any other domain registrars to maintain the public records. The
                information provided as is without any warranty and for general usage.</p>
            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
