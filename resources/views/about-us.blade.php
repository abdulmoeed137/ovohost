@extends('layouts.app')

@section('title', 'About Us | OvoHost')

@section('styles')
<style>
                #highlights {
                    margin-top: 30px;
                    margin-bottom: 20px;
                }

                .highlight-block {
                    width: 30%;
                    margin-right: 3.33%;
                    float: left;
                }

                .high {
                    font-size: 35px;
                    text-align: center;
                    color: #0e5077;
                    font-family: Lato, sans-serif;
                    font-weight: bold;
                    margin-bottom: 10px;
                }

                .low {
                    font-size: 15px;
                    text-align: center;
                }

                @media only screen and (max-width : 767px) {
                    .highlight-block {
                        width: 100%;
                        float: none;
                        margin-bottom: 20px;
                    }
                }
            </style>
@endsection
@section('content')

<div id="headline">
        <h1>About OvoHost</h1>
    </div>

    <div class="content-adj">
            <p>OvoHost.pk is a fast growing web hosting service provider in Saudi Arabia, offering services since 2009 with
                experience of managing 10,000+ websites. Our services include global and .com domains registration,
                reliable web hosting and web development.</p>
            <br>
            <p>Our hosting comes with hundreds of features including free domain, free site builder, rock solid 99.9%
                uptime, free domain name & 30 days money back guarantee – so you can have peace of mind! We have clients
                for Web Hosting in Riyadh, Al-Kharj, Jeddah and all other major cities in Saudi Arabia, give us a try now!
            </p>
            <br>
            <p>We at OvoHost ensure that we deliver the highest quality hosting service with the lowest possible price
                to our customers – this means you get high standard cPanel hosting at a much lower price compared to
                martket! Our hosting plans are designed for individuals and small businesses to host their websites and
                not worry about spending huge cost for setup!</p>


            <h3
                style="text-align: center; font-size: 25px; margin-top: 30px; margin-bottom: 20px; padding-top:20px; border-top:1px solid #000">
                OvoHost's Performance in 2018:
            </h3>

            <p style="text-align: center;">
                Here is the quick snapshot of services we delivered to our customers last year, that alot hard work put
                into action!
            </p>

           

            <div id="highlights">
                <div class="highlight-block">
                    <p class="high">10+ Million</p>
                    <p class="low">Visitors Served!</p>
                </div>

                <div class="highlight-block">
                    <p class="high">2+ Million</p>
                    <p class="low">Emails Sent!</p>
                </div>

                <div class="highlight-block">
                    <p class="high">99.91%</p>
                    <p class="low">Uptime Recorded!</p>
                </div>

                <div class="clear"></div>
            </div>

            <p style="text-align: center;">
                In 2019, OvoHost is all set to remain top Web Hosting Company in Saudi Arabia.
                <br>
                Take a look at our <a href="index.html">Web Hosting Packages</a> now.
            </p>

            <div class="clear"></div>

            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection