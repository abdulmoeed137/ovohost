@extends('layouts.app')

@section('title', 'Frequently Asked Questions | OvoHost')



@section('content')

<div id="headline">
<h1>FAQs</h1>
        </div>

        <div class="content-adj">
            <div id="faq">

                <h3 style="margin-bottom: 10px">General Questions</h3>

                <p class="accordion">How to order hosting with ovohost.com?</p>
                <div class="panel">
                    Step # 1: Choose web hosting plan & click 'Order' button to proceed.<br>
                    Step # 2: Select new domain, transfer domain to us or use an existing domain to continue.<br>
                    Step # 3: Select billing duration (yearly, bi-annually) and submit promo codes (leave it empty if
                    you don't know).<br>
                    Step # 4: Submit personal details on checkout page to complete your order.<br>
                    Step # 5: Send payment and get your account activated.
                </div>

                <p class="accordion">How to send payment against an invoice?</p>
                <div class="panel">
                    You have multiple options to send payment to us, you can pay through:<br>
                    - Bank transfer (SCB & UBL)<br>
                    - Jazzcash<br>

                    - Easypaisa<br>
                    Exact account details are sent in invoice once you submit your order.<br>
                    Once you complete payment send confirmation email to info@ovohost.com and we will activate your
                    account.
                </div>

                <p class="accordion">How much time will it take to activate my account after payment?</p>
                <div class="panel">We activate all accounts within 30 minutes (on an average) after you have completed
                    your payment and notify us through email or whatsapp.</div>

                <p class="accordion">Can you design / develop website for me?</p>
                <div class="panel">Yes, sure! Send us your requirements at info@ovohost.com and we will send you a quote.
                </div>

            </div>

           


            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection

@section('scripts')
<script>
                document.addEventListener("DOMContentLoaded", function (event) {


                    var acc = document.getElementsByClassName("accordion");
                    var panel = document.getElementsByClassName('panel');

                    for (var i = 0; i < acc.length; i++) {
                        acc[i].onclick = function () {
                            var setClasses = !this.classList.contains('active');
                            setClass(acc, 'active', 'remove');
                            setClass(panel, 'show', 'remove');

                            if (setClasses) {
                                this.classList.toggle("active");
                                this.nextElementSibling.classList.toggle("show");
                            }
                        }
                    }

                    function setClass(els, className, fnName) {
                        for (var i = 0; i < els.length; i++) {
                            els[i].classList[fnName](className);
                        }
                    }

                });
</script>
@endsection