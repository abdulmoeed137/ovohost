@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/all.min6892.css?v=db9674') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style-whmcs.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #ffffff;
        }

        .row:first {
            margin-top: 2rem;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9 pull-md-right">
            <div class="header-lined">
                <h1>Register <small>Create an account with us . . .</small></h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.html"> Portal Home
                        </a>
                    </li>
                    <li class="active">
                        Register
                    </li>
                </ol>
            </div>
        </div>
        <div class="col-md-3 pull-md-left sidebar">
            <div menuItemName="Already Registered" class="panel panel-sidebar panel-sidebar">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>&nbsp; Already Registered?
                        <i class="fas fa-chevron-up panel-minimise pull-right"></i>
                    </h3>
                </div>
                <div class="list-group">
                    <div menuItemName="Already Registered Heading" class="list-group-item"
                        id="Primary_Sidebar-Already_Registered-Already_Registered_Heading">
                        Already registered with us? If so, click the button below to login to our client area from
                        where
                        you
                        can manage your account.
                    </div>
                    <a menuItemName="Login" href="index804a.html" class="list-group-item"
                        id="Primary_Sidebar-Already_Registered-Login">
                        <i class="fas fa-user"></i>&nbsp; Login
                    </a>
                    <a menuItemName="Lost Password Reset" href="index0839.html?rp=/password/reset" class="list-group-item"
                        id="Primary_Sidebar-Already_Registered-Lost_Password_Reset">
                        <i class="fas fa-asterisk"></i>&nbsp; Lost Password Reset
                    </a>
                </div>
            </div>
        </div>
        <!-- Container for main page display content -->
        <div class="col-md-9 pull-md-right main-content">
            <div id="registration">
                <form method="post" class="using-password-strength" action="https://www.easyhost.pk/account/register.php"
                    role="form" name="orderfrm" id="frmCheckout">
                    <input type="hidden" name="token" value="9cbdad5cdb5248a300bd8e2e56822d409fca3b68" />
                    <input type="hidden" name="register" value="true" />

                    <div id="containerNewUserSignup">
                        <div id="providerLinkingMessages" class="hidden">
                            <p class="providerLinkingMsg-preLink-init_failed">
                                <span class="provider-name"></span> is unavailable at this time. Please try again
                                later.
                            </p>
                            <p class="providerLinkingMsg-preLink-connect_error">
                                <strong>Error</strong> We were unable to connect your account. Please contact your
                                system
                                administrator.
                            </p>
                            <p class="providerLinkingMsg-preLink-complete_sign_in">
                                Please complete sign in with your chosen service provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-2fa_needed">
                                Automatic sign-in successful! Redirecting...
                            </p>
                            <p class="providerLinkingMsg-preLink-linking_complete">
                                <strong>Success!</strong> Your account is now linked with your :displayName account.
                            </p>
                            <p class="providerLinkingMsg-preLink-login_to_link-signin-required">
                                <strong>Link Initiated!</strong> Please complete sign in to associate this service
                                with
                                your
                                existing account. You will only have to do this once.
                            </p>
                            <p class="providerLinkingMsg-preLink-login_to_link-registration-required">
                                <strong>Link Initiated!</strong> Please complete the registration form below.
                            </p>
                            <p class="providerLinkingMsg-preLink-checkout-new">
                                <strong>Link Initiated!</strong> Please complete your new account information.
                            </p>
                            <p class="providerLinkingMsg-preLink-other_user_exists">
                                <strong>Error</strong> This account is already connected to an existing account with
                                us.
                                Please choose a different account at the third party authentication provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-already_linked">
                                <strong>Error</strong> This account is already connected to your account with us.
                                Please
                                choose a different account at the third party authentication provider.
                            </p>
                            <p class="providerLinkingMsg-preLink-default">
                                <strong>Error</strong> We were unable to connect your account. Please contact your
                                system
                                administrator.
                            </p>
                        </div>

                        <div class="sub-heading">
                            <span>Sign Up</span>
                        </div>

                        <div class="providerPreLinking" data-link-context="registration" data-hide-on-prelink=1
                            data-disable-on-prelink=0>
                            <div class="social-signin-btns">
                                <button class="btn btn-social btn-facebook fb-login-button" data-max-rows="1"
                                    data-size="medium" data-button-type="login_with" data-show-faces="false"
                                    data-auto-logout-link="false" data-use-continue-as="false"
                                    data-scope="public_profile,email" onclick="onLoginClick()" type="button">
                                    <i class="fab fa-facebook"></i>
                                    Sign Up with Facebook
                                </button>
                            </div>
                        </div>

                        <div class="providerLinkingFeedback"></div>


                        <div class="sub-heading">
                            <span>Personal Information</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputFirstName" class="field-icon">
                                        <i class="fas fa-user"></i>
                                    </label>
                                    <input type="text" name="firstname" id="inputFirstName" class="field form-control"
                                        placeholder="First Name" value="" required autofocus>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputLastName" class="field-icon">
                                        <i class="fas fa-user"></i>
                                    </label>
                                    <input type="text" name="lastname" id="inputLastName" class="field form-control"
                                        placeholder="Last Name" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputEmail" class="field-icon">
                                        <i class="fas fa-envelope"></i>
                                    </label>
                                    <input type="email" name="email" id="inputEmail" class="field form-control"
                                        placeholder="Email Address" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputPhone" class="field-icon">
                                        <i class="fas fa-phone"></i>
                                    </label>
                                    <input type="tel" name="phonenumber" id="inputPhone" class="field"
                                        placeholder="Phone Number" value="">
                                </div>
                            </div>
                        </div>

                        <div class="sub-heading">
                            <span>Billing Address</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group prepend-icon">
                                    <label for="inputCompanyName" class="field-icon">
                                        <i class="fas fa-building"></i>
                                    </label>
                                    <input type="text" name="companyname" id="inputCompanyName" class="field"
                                        placeholder="Company Name (Optional)" value="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group prepend-icon">
                                    <label for="inputAddress1" class="field-icon">
                                        <i class="far fa-building"></i>
                                    </label>
                                    <input type="text" name="address1" id="inputAddress1" class="field form-control"
                                        placeholder="Street Address" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group prepend-icon">
                                    <label for="inputAddress2" class="field-icon">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </label>
                                    <input type="text" name="address2" id="inputAddress2" class="field"
                                        placeholder="Street Address 2" value="">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group prepend-icon">
                                    <label for="inputCity" class="field-icon">
                                        <i class="far fa-building"></i>
                                    </label>
                                    <input type="text" name="city" id="inputCity" class="field form-control"
                                        placeholder="City" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group prepend-icon">
                                    <label for="state" class="field-icon" id="inputStateIcon">
                                        <i class="fas fa-map-signs"></i>
                                    </label>
                                    <label for="stateinput" class="field-icon" id="inputStateIcon">
                                        <i class="fas fa-map-signs"></i>
                                    </label>
                                    <input type="text" name="state" id="state" class="field form-control"
                                        placeholder="State" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group prepend-icon">
                                    <label for="inputPostcode" class="field-icon">
                                        <i class="fas fa-certificate"></i>
                                    </label>
                                    <input type="text" name="postcode" id="inputPostcode" class="field form-control"
                                        placeholder="Postcode" value="" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group prepend-icon">
                                    <label for="inputCountry" class="field-icon" id="inputCountryIcon">
                                        <i class="fas fa-globe"></i>
                                    </label>
                                    <select name="country" id="inputCountry" class="field form-control">
                                        <option value="AX">
                                            Aland Islands
                                        </option>
                                        <option value="AL">
                                            Albania
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="sub-heading">
                            <span>Additional Required Information</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="customfield3">How did you find us?</label>
                                    <div class="control">
                                        <select name="customfield[3]" id="customfield3" class="form-control">
                                            <option value="">None</option>
                                            <option value="Google">Google</option>
                                            <option value="Facebook">Facebook</option>
                                            <option value="Friend">Friend</option>
                                            <option value="Newspaper">Newspaper</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputCurrency" class="field-icon">
                                        <i class="far fa-money-bill-alt"></i>
                                    </label>
                                    <select id="inputCurrency" name="currency" class="field form-control">
                                        <option value="1" selected>PKR</option>
                                        <option value="2">USD</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="containerNewUserSecurity">

                        <div class="sub-heading">
                            <span>Account Security</span>
                        </div>
                        <div id="containerPassword" class="row">
                            <div id="passwdFeedback" style="display: none;" class="alert alert-info text-center col-sm-12">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputNewPassword1" class="field-icon">
                                        <i class="fas fa-lock"></i>
                                    </label>
                                    <input type="password" name="password" id="inputNewPassword1" data-error-threshold="50"
                                        data-warning-threshold="75" class="field" placeholder="Password"
                                        autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group prepend-icon">
                                    <label for="inputNewPassword2" class="field-icon">
                                        <i class="fas fa-lock"></i>
                                    </label>
                                    <input type="password" name="password2" id="inputNewPassword2" class="field"
                                        placeholder="Confirm Password" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-sm btn-xs-block generate-password"
                                        data-targetfields="inputNewPassword1,inputNewPassword2">
                                        Generate Password
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="password-strength-meter">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped"
                                            role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                            id="passwordStrengthMeterBar">
                                        </div>
                                    </div>
                                    <p class="text-center small text-muted" id="passwordStrengthTextLabel">Password
                                        Strength: Enter a Password</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-danger tospanel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fas fa-exclamation-triangle tosicon"></span>
                                        &nbsp; Terms of Service</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <label class="checkbox">
                                            <input type="checkbox" name="accepttos" class="accepttos">
                                            I have read and agree to the <a href="../tos.html" target="_blank">Terms
                                                of
                                                Service</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p align="center">
                        <input class="btn btn-large btn-primary btn-recaptcha btn-recaptcha-invisible" type="submit"
                            value="Register" />
                    </p>
                </form>
            </div>


        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/scripts.min6892.js?v=db9674') }}"></script>
    <script type="text/javascript" src="{{ asset('js/StatesDropdown.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/PasswordStrength.js') }}"></script>
    <script>
        window.langPasswordStrength = "Password Strength";
        window.langPasswordWeak = "Weak";
        window.langPasswordModerate = "Moderate";
        window.langPasswordStrong = "Strong";
        $(document).ready(function() {
            $("#inputNewPassword1").keyup(registerFormPasswordStrengthFeedback);
        });

        window.onerror = function(e) {
            WHMCS.authn.provider.displayError();
        };

        window.fbAsyncInit = function() {
            FB.init({
                appId: "243984456286963",
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true, // parse social plugins on this page
                version: "v2.8" // use graph api version 2.8
            });
        };

        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "../../connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, "script", "facebook-jssdk"));

        function onLoginClick() {
            WHMCS.authn.provider.preLinkInit();

            FB.login(
                function(response) {
                    var feedbackContainer = jQuery(".providerLinkingFeedback");
                    var btnContainer = jQuery(".providerPreLinking");

                    var failIfExists = 0;
                    if ("register" === "register" ||
                        "register" === "connect"
                    ) {
                        failIfExists = 1;
                    }

                    var context = {
                        htmlTarget: "register",
                        targetLogin: "login",
                        targetRegister: "register",
                        redirectUrl: "%2Faccount%2Fclientarea.php"
                    };

                    if (response.status === 'connected') {
                        var config = {
                            url: "/account/index.php?rp=/auth/provider/facebook_signin/finalize",
                            method: "POST",
                            dataType: "json",
                            data: {
                                accessToken: response.authResponse.accessToken,
                                fail_if_exists: failIfExists,
                                token: "9cbdad5cdb5248a300bd8e2e56822d409fca3b68",
                                cartCheckout: 0
                            }
                        };
                        var provider = {
                            name: "Facebook",
                            icon: "<i class=\"fab fa-facebook\"></i> "
                        };

                        var providerDone = function() {
                            FB = null;
                        };
                        var providerError = function() {};
                    } else if (!response.status) {
                        response.status = "unknown";
                    }

                    switch (response.status) {
                        case "connected":
                            WHMCS.authn.provider.signIn(config, context, provider, providerDone, providerError);
                            break;
                        case "not_authorized":
                            feedbackContainer.text(
                                'You did not authorize the use of Facebook for authentication. We can\'t use it to log you in.'
                            ).slideDown();
                            break;
                        case "unknown":
                            feedbackContainer.slideUp();
                    }
                });
        }
    </script>
@endsection
