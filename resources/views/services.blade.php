@extends('layouts.app')

@section('title', 'Services | OvoHost')

@section('styles')

<style>
                .serv-box {
                    margin-top: 0px;
                    margin-bottom: 20px;
                    background: #f9f9f9;
                    width: 30.33%;
                    float: left;
                    margin-right: 1.5%;
                    margin-left: 1.5%
                }

                .serv-padding {
                    padding: 20px
                }

                .serv-box ul {
                    margin-top: 5px;
                    margin-bottom: 5px;
                    margin-left: 20px;
                }

                .serv-box ul li {
                    list-style-image: url('images/tick-icon.png');
                    line-height: 25px;
                    font-size: 15px
                }

                .serv-box h3 {
                    line-height: 30px;
                    font-size: 22px;
                    margin-bottom: 5px;
                    text-align: center
                }

                .serv-box p {
                    font-size: 13px;
                    line-height: 25px;
                }

                .special {
                    font-size: 17px;
                    color: green;
                    font-weight: bold;
                    margin-bottom: 5px;
                    text-align: center
                }

                .serv-box a {
                    margin: 10px 0px;
                    font-weight: bold;
                    color: white;
                    font-size: 10pt;
                    border-radius: 5px;
                    padding: 10px 15px;
                    display: inline-block;
                    text-decoration: none;
                    background-color: #0e5077;
                }
            </style>
@endsection

@section('content')

<div id="headline">
<h1>Services</h1>
        
        </div>

        <div class="content-adj">
            <p style="padding:0px 20px">
            OvoHost.COM is a fast growing <a href="index.html">Web Hosting provider in Saudi Arabia</a>, offering
                services since 2009 with experience of managing 10,000+ websites. Our services include global and .COM
                domains registration, reliable web hosting and web development.
                <br><br>
            </p>

           

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>Shared Hosting</h3>
                    <p class="special">Starting from SR.1,500/year</p>
                    <p>
                        Our shared hosting plans are designed to meet the needs of Personal and low traffic websites
                        that are in the process of growth.
                        We offer affordable Web Hosting plans with:
                        <ul>
                            <li>Free Domain for first year</li>
                            <li>Free SSL Ceriticate</li>
                            <li>Wordpress supported</li>
                            <li>Ready in less than 30 minutes</li>
                        </ul>
                        <a href="{{route('host','web')}}">
                            Web Hosting Saudi Arabia Plans
                        </a>
                    </p>
                </div>
            </div>

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>Unlimited Hosting</h3>
                    <p class="special">Starting from SR.3,000/month</p>
                    <p>
                        Our unlimited plans are designed to meet the needs websites that require unlimited features
                        (email, databases etc), with a powerful 10 GB SSD disk.
                        Unlimited Web Hosting plans include:
                        <ul>
                            <li>Free Domain for first year</li>
                            <li>Free SSL Ceriticate</li>
                            <li>Wordpress, Ecommerce supported</li>
                            <li>Ready in less than 30 minutes</li>
                        </ul>
                        <a href="{{route('host','unlimited')}}">
                            Unlimited Hosting Plans
                        </a>
                    </p>
                </div>
            </div>

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>Reseller Hosting</h3>
                    <p class="special">Starting from SR.500/month</p>
                    <p>
                        With reseller plans, you can offer cPanel based hosting to your own clients or you can create
                        accounts for your own different sites.
                        Reseller plans include:
                        <ul>
                            <li>Free Domain with annual payment</li>
                            <li>Free SSL Ceriticate</li>
                            <li>WHM / cPanel Accounts</li>
                            <li>Sell hosting to your clients</li>
                        </ul>
                        <a href="{{route('host','reseller')}}">
                            Reseller Hosting Plans
                        </a>
                    </p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>Business Hosting</h3>
                    <p class="special">Starting from SR.5,000/year</p>
                    <p>
                        Business class hosting is for corporate / business critical sites that require 100% Uptime, Rock
                        Solid server and speed!
                        Plans include:
                        <ul>
                            <li>Enterprise SSD drives</li>
                            <li>Free Domain, SSL Ceriticate</li>
                            <li>100% Uptime, Local backups</li>
                            <li>DDOS Protection</li>
                        </ul>
                        <a href="{{route('host','business')}}">
                            Business Hosting Plans
                        </a>
                    </p>
                </div>
            </div>

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>Domain Registration</h3>
                    <p class="special">Starting from SR.1,000/year</p>
                    <p>
                    OvoHost offer cheap domain registration services in Saudi Arabia, we follow the top class Public
                        Domain Registry to offer domain services to clients.
                        <ul>
                            <li>.COM for SR.1,000/yr</li>
                            <li>.NET for SR.1,000/yr</li>
                            <li>.CO for SR.2,900/yr</li>
                            <li>Other global extenstions</li>
                        </ul>
                        <a href="{{route('domain')}}">
                            Domain Registration Prices
                        </a>
                    </p>
                </div>
            </div>

            <div class="serv-box">
                <div class="serv-padding">
                    <h3>.COM Domain Registration</h3>
                    <p class="special">.COM Domains for SR.2,200/2year</p>
                    <p>
                    OvoHost offers hassle free .COM domain registration & management - we register, maintain and
                        renew your domains.
                        <ul>
                            <li>.COM - best for personal / business sites</li>
                            <li>.COM.PK - best for companies</li>
                            <li>.NET.PK - best for businesses and ISPs</li>
                            <li>.EDU.PK - for Educational Institutes</li>
                        </ul>
                        <a href="{{route('pkdomain')}}">
                            .COM Domain Prices
                        </a>
                    </p>
                </div>
            </div>

            <div class="clear"></div>


            <div id="aboutus">


                <h3>About OvoHost</h3>


                <p>We offer web hosting and domain registration services in Saudi Arabia for individuals and businesses so
                    they can
                    setup their website in less than an hour. With out cPanel enabled hosting packages you can start
                    branding
                    your business on internet faster than ever before! Our hosting comes with hundreds of features
                    including a
                    rock solid 99.9% uptime, free domain name & 30 days money back guarantee - so you don't have to
                    worry about
                    your money! </p>


                <table width="80%" style="margin: 20px auto;">


                    <tr>


                        <td width="33%" style="text-align: center;"><img src="images/money-back-guarantee.gif"
                                alt="money back guarantee" height="140"></td>


                        <td width="33%" style="text-align: center"><img src="images/uptime-guarantee.png"
                                alt="uptime guarantee" height="140"></td>


                        <td width="33%" style="text-align: center"><img src="images/satisfaction-guarantee.png"
                                alt="satisfaction guarantee" height="140"></td>


                    </tr>


                    <tr>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">30 Days
                            Money Back
                            Guarantee - so you can try hassle free!
                        </td>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">You get
                            99.9% uptime
                            on our USA & UK based servers!
                        </td>


                        <td style="text-align: center; font-size: 14px; font-weight: bold; padding: 0 10px">We go extra
                            mile for
                            customer's satisfaction whenever you need!
                        </td>


                    </tr>


                </table>


            </div>



            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
