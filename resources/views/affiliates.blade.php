@extends('layouts.app')

@section('title', 'Affiliates | Earn Money Online in Saudi Arabia | OvoHost')

<!-- @section('styles')
<style>
                #highlights {
                    margin-top: 30px;
                    margin-bottom: 20px;
                }

                .highlight-block {
                    width: 30%;
                    margin-right: 3.33%;
                    float: left;
                }

                .high {
                    font-size: 35px;
                    text-align: center;
                    color: #0e5077;
                    font-family: Lato, sans-serif;
                    font-weight: bold;
                    margin-bottom: 10px;
                }

                .low {
                    font-size: 15px;
                    text-align: center;
                }

                @media only screen and (max-width : 767px) {
                    .highlight-block {
                        width: 100%;
                        float: none;
                        margin-bottom: 20px;
                    }
                }
            </style>
@endsection -->
@section('content')

<div id="headline">
        <h1>Affiliates</h1>
    </div>

    <div class="content-adj">
            <h3 style="text-align: center; font-size: 40px; margin-bottom: 10px">Earn Money Online</h3>

            <p style="line-height: 30px">

                You earn money by referring customer to OvoHost.com, through our affiliate program! This is great chance
                for you to start earning through your websites.
                <br><br>
                <strong>How does affiliate program works?</strong>
                <br>
                You can sign up as an affiliate with us by following the below given steps:
                <ul style="line-height: 25px; margin-left: 20px; margin-bottom: 10px">
                    <li>Login to clientarea through: <a
                            href="account/index804a.html">https://www.easyhost.pk/account/clientarea.php</a></li>
                    <li>Click on &#8220;Affiliates&#8221; from the top menu</li>
                    <li>Click on &#8220;Activate Affiliate Account&#8221; to get started with the program</li>
                    <li>Your affiliate program is active now! Your personal affiliate link will look like: <a
                            href="index.html?aff=1">https://www.easyhost.pk/account/aff.php?aff=1</a></li>
                </ul>

                <p style="line-height: 30px">
                    <i>Start sending this link to your friends and family, when they make a purchase you get upto 20%
                        commission on each sale!</i>
                    <br><br>
                    <strong>How do you get paid?</strong>
                    <br>
                    > You will receive Rs.250 affiliate sign up bonus in your affiliate program and you will receive
                    upto 20% commission for each sale you refer (this commission will be added to your affiliate
                    account).
                    <br>
                    > Once your affiliate commissions reach Rs.1500, you will be able to request withdrawal by clicking
                    &#8220;Request Withdrawal&#8221; from affiliates page.
                    <br>
                    > You can ask us to send affiliate earning to your Jazzcash / Easypaisa account (transferred within
                    48 hours of your request) or you can ask for a voucher / account balance to pay existing invoices.
                    <br><br>
                    In case you require any help, please reach us at info@ovohost.com

                </p>


                <div class="buttonset">
                    <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
                    <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM
                        US</a>
                    <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
                </div>

                <div class="clear"></div>

                <div class="clear"></div>
    </div>
@endsection