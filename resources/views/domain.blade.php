@extends('layouts.app')

@section('title', 'Domain Registration Saudi Arabia | .COM Domain | OvoHost')

@section('styles')
    <style>
        #leftpay {
            float: left;
            width: 49%
        }

        #rightpay {
            float: right;
            width: 49%
        }

        @media only screen and (max-width: 767px) {
            #leftpay {
                float: none;
                width: 100%
            }

            #rightpay {
                float: none;
                width: 100%;
                margin-top: 20px
            }
        }

        #pkdomain {
            text-align: center;
            font-size: 14px;
            border-right: 1px solid #e1e1e1
        }

        #pkdomain tr td {
            background: #f9f9f9;
            padding: 6px;
            border-bottom: 1px solid #e1e1e1;
            border-left: 1px solid #e1e1e1
        }

        #pkdomain thead td {
            background-color: #0e5077;
            color: #fff;
            padding: 10px;
            font-weight: bold;
        }

        @media only screen and (max-width : 767px) {
            #pkdomain {
                text-align: center;
                font-size: 12px
            }

            #pkdomain tr td {
                background: #f9f9f9;
                padding: 5px
            }

            #pkdomain thead td {
                background-color: #0e5077;
                color: #fff;
                padding: 5px;
                font-weight: bold;
            }
        }

        .do-checker {
            background-color: #f9f9f9;
            margin-bottom: 10px
        }

        .search_div input {
            padding: 5px;
            font-size: 20px;
        }

        .submit_div input {
            padding: 5px 30px;
            font-size: 20px;
            color: #0c3754;
            font-weight: bold;
            margin-top: 10px
        }

        .select_div select {
            padding: 5px;
            font-size: 20px;
            margin-top: 10px
        }

        .domainsbox {
            width: 21%;
            float: left;
            background-color: #f9f9f9;
            margin-left: 2%;
            margin-right: 2%;
            margin-top: 10px;
            margin-bottom: 20px
        }

        .domainscircle {
            margin: 10px auto;
            border-radius: 50%;
            background: #0e5077;
            width: 150px;
            height: 150px;
            text-align: center;
            color: white;
            font-weight: bold;
        }

        .domainscircle h3 {
            padding-top: 50px;
            font-size: 22px
        }

        .domainscircle p {
            font-size: 15px !important;
        }

        .domainsbox p {
            text-align: center;
            padding: 0px 10px;
            font-size: 13px;
        }

        .lfamous {
            padding: 5px;
            background-color: darkorange;
            font-weight: bold;
            color: white
        }

        .searchref {
            padding: 5px;
            background-color: #0e5077;
            color: white;
            text-decoration: none
        }

        @media only screen and (max-width : 767px) {
            .domainsbox {
                width: 100%
            }
        }

    </style>
@endsection

@section('content')
    <div id="headline">
        <h1>Domain Registation in Saudi Arabia</h1>
    </div>
    <div class="content-adj">
        <p style="margin-bottom: 20px; font-size: 14px">
        OvoHost gives you full control over domain names i.e. you can manage domain DNS, registrar lock and generate
            EPP code from within our billing panel (manual for unsupported extensions).
            We offer free domain registration with every web hosting plan, check our <a href="web-hosting.html">web
                hosting</a> plans for details.
        </p>

        <div id="leftpay">
            <table width="100%%" cellpadding="0" cellspacing="0" id="pkdomain">
                <thead>
                    <td width="40%" class="lalign">Domains</td>
                    <td width="20%">Registration</td>
                    <td width="20%">Renewal</td>
                    <td width="20%">Transfer</td>
                </thead>
                <tr>
                    <td class="lalign">.COM</td>
                    <td>SR.1700/yr</td>
                    <td>SR.1700/yr</td>
                    <td>SR.1700/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.NET</td>
                    <td>SR.2400/yr</td>
                    <td>SR.2400/yr</td>
                    <td>SR.2400/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.ORG</td>
                    <td>SR.2100/yr</td>
                    <td>SR.2100/yr</td>
                    <td>SR.2100/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.INFO</td>
                    <td>SR.2400/yr</td>
                    <td>SR.2400/yr</td>
                    <td>SR.2400/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.BIZ</td>
                    <td>SR.2000/yr</td>
                    <td>SR.2000/yr</td>
                    <td>SR.2000/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.US</td>
                    <td>SR.1200/yr</td>
                    <td>SR.1200/yr</td>
                    <td>SR.1200/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.CO.UK</td>
                    <td>SR.2800/yr</td>
                    <td>SR.2800/yr</td>
                    <td>SR.2800/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.CO</td>
                    <td>SR.5000/yr</td>
                    <td>SR.5000/yr</td>
                    <td>SR.5000/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.AE</td>
                    <td>SR.7500/yr</td>
                    <td>SR.7500/yr</td>
                    <td>SR.7500/yr</td>
                </tr>
                <tr>
                    <td class="lalign">.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.COM.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.NET.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.ORG.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.BIZ.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.WEB.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.EDU.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td class="lalign">.GOV.PK (2 years)</td>
                    <td>SR.2350/2yr</td>
                    <td>SR.2350/2yr</td>
                    <td>-</td>
                </tr>
            </table>

        </div>

        <div id="rightpay" style="background: #f9f9f9; text-align: center; border: 1px solid #e1e1e1">

            <h3 style="line-height: 40px; background-color: #0e5077; color: white" id="searchpk">Search Domain</h3>

            <p style="margin: 20px 0; font-weight: bold">Search Domain for your Personal or Company Website:</p>

            <div class='do-checker'>
                <form method="post" action='https://www.easyhost.pk/account/cart.php?a=add&amp;domain=register'>
                    <div class=" search_div">
                        <input required='required' title='Please fill out this field' type="search" name="sld"
                            id="search_domain" placeholder="Search .pk domains" value="" />
                    </div>
                    <div class="select_div">
                        <select name='tld'>
                            <option>.com</option>
                            <option>.net</option>
                            <option>.org</option>
                            <option>.info</option>
                            <option>.biz</option>
                            <option>.us</option>
                            <option>.co</option>
                            <option>.co.uk</option>
                            <option>.ae</option>
                            <option>.pk</option>
                            <option>.com.pk</option>
                            <option>.net.pk</option>
                            <option>.org.pk</option>
                            <option>.biz.pk</option>
                            <option>.edu.pk</option>
                            <option>.gov.pk</option>
                            <option>.web.pk</option>
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div class="submit_div"><input type="submit" value="Search Domain"></div>
                </form>
                <div class="clear"></div>
            </div>

            <p style="margin-bottom: 10px; margin-top: 20px">
                <i>
                    Need Web Hosting as well? Check our <a href="web-hosting.html">Shared Hosting Plans</a>
                    <br>
                    Our web hosting plans include free .COM domain registration.
                </i>
            </p>

        </div>

        <div class="clear"></div>

        <h3 style="text-align: center; font-size: 25px; margin-top: 30px; margin-bottom: 20px">.PK Domains are best for
            Saudi Arabia Origin websites:</h3>

        <p style="text-align: center; margin-bottom: 20px">Find details about all available .PK domain extensions and their
            general registration guide:</p>

        <div class="domainsbox">
            <p class="lfamous">Most Used Extension!</p>
            <div class="domainscircle">
                <h3>.COM</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Most Used Extension!</p>
            <div class="domainscircle">
                <h3>.COM.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Best for Network & ISPs</p>
            <div class="domainscircle">
                <h3>.NET.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Networking bodies & ISPs.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">Best for Organizations / NGOs</p>
            <div class="domainscircle">
                <h3>.ORG.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Organizations & Not-for-Profit cause.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="clear"></div>

        <div class="domainsbox">
            <p class="lfamous">General / Business Use</p>
            <div class="domainscircle">
                <h3>.BIZ.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal & Business websites.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">General / Business Use</p>
            <div class="domainscircle">
                <h3>.WEB.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>Open for everyone to register.<br>Perfect for Personal, Businesses & Companies.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">For Edu. Institutes</p>
            <div class="domainscircle">
                <h3>.EDU.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>For registered institutes to register.<br>Registered academies / schools and education centers use.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="domainsbox">
            <p class="lfamous">For Gov. Organizations</p>
            <div class="domainscircle">
                <h3>.GOV.PK</h3>
                <p>SR.2,350/2yr</p>
            </div>
            <p>For Registerd GOV. Org. to register.<br>Government departments, organizations can register.</p>
            <p style="margin-bottom: 10px; margin-top: 5px"><a href="#searchpk" class="searchref">Search Domain</a></p>
        </div>

        <div class="clear"></div>

        <p style="line-height: 30px; text-align: center">
            Need help? <a href="{{ route('contact') }}">Contact Us Now!</a>
        </p>

        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #e68e35" href="{{ route('host','web') }}">GET WEB HOSTING FROM US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>
        <div class="clear"></div>
    </div>
@endsection
