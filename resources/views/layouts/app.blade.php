<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description"
        content="OvoHost is the fastest growing Web Hosting provider in Pakistan, offering reliable Website Hosting service to 5,000+ customers in Riyadh, Al-Kharj & Jeddah." />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href="index.html" />
    <link rel="icon" type="image/png" href="{{ asset('images/fav.png') }}" />
    <link rel="dns-prefetch" href="http://s.w.org/" />

    <meta property="og:title" content="OvoHost.com - #1 Web Hosting" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="index.html" />
    <meta property="og:image" content="../ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/main-social.jpg" />
    <meta property="og:image:secure_url" content="../ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/main-social.jpg" />
    <meta property="og:description"
        content="OvoHost is the fastest growing Web Hosting provider in Pakistan, offering reliable Website Hosting service to 5,000+ customers in Riyadh, Al-Kharj & Jeddah." />

    <title>@yield('title','OvoHost: Web Hosting Saudi Arabia, .Com Domain Registration, Website Hosting Company')</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #customers {
            width: 100%;
            background-color: #f3f3f3;
            margin-top: 20px;
            padding-bottom: 20px
        }

        #customers h3 {
            padding: 30px 50px 10px 50px;
            font-size: 25px
        }

        #customers p {
            padding: 0px 50px;
            font-size: 17px;
            padding-bottom: 10px
        }

        #customers ul {
            margin: 0px 50px
        }

        #customers ul li {
            list-style: none;
            float: left;
            font-size: 16px;
            line-height: 30px;
            width: 220px
        }

        #customers ul li a {
            color: blue
        }

        .green {
            color: green;
            font-weight: bold;
        }

        .boldi {
            font-weight: bold
        }

        .price-option {
            padding: 2px;
            width: 95%;
            margin: 5px auto 0px auto;
            text-align: center !important;
        }

        select {
            text-align: center;
        }

        .order-b-form {
            margin: 0 auto;
            font-weight: bold;
            color: white;
            font-size: 10pt;
            border-radius: 3px;
            padding: 7px 10px;
            background-color: #154a69;
            border: 0px;
            box-shadow: 0 3px 0 #000, 0 6px 4px -2px rgba(0, 0, 0, 0.3);
            cursor: pointer !important;
        }

        .col_one_third {
            width: 33.33%;
            float: left;
            text-align: center;
        }

        .col_one_third h3 {
            background-color: #0e5077;
            padding: 5px;
            color: white
        }

        .col_one_third p {
            line-height: 30px;
            padding: 0px 15px
        }

        @media only screen and (max-width : 767px) {
            .col_one_third {
                width: 100%;
            }
        }

        .services {
            width: 100%;
            background-color: #f3f3f3;
            padding-top: 20px;
            padding-bottom: 20px;
            margin-bottom: 30px
        }

        .services_left {
            width: 60%;
            float: left
        }

        .services_right {
            width: 40%;
            float: right
        }

        .services_right img {
            width: 400px
        }

        .services h3 {
            font-size: 30px;
            margin-bottom: 20px;
            margin-top: 10px
        }

        .services p {
            line-height: 30px
        }

        .services ul {
            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 30px
        }

        .services ul li {
            float: left;
            width: 50%;
            line-height: 30px;
            list-style-image: url('../ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/ricon.png')
        }

        .services a {
            border: 0px;
            color: white;
            background-color: #0e5077;
            text-decoration: none
        }

        .services_padding {
            padding: 30px
        }

        @media only screen and (max-width : 767px) {
            .services_left {
                width: 100%;
                float: none
            }

            .services_right {
                width: 100%;
                text-align: center;
                float: none
            }

            .services ul li {
                width: 100%
            }

            .services_right img {
                width: 250px
            }
        }

        #footertop {
            width: 100%;
            float: left;
            padding-top: 20px;
            padding-bottom: 10px
        }

        #footertop figure {
            float: left;
            margin-right: 20px;
            margin-top: 10px;
            margin-bottom: 20px;
        }

        #footertop dl {
            display: block;
        }

        #footertop dl dt {
            font-size: 18px;
            font-weight: 700;
            color: #fff;
            margin-bottom: 10px;
        }

        #footertop dl dd {
            color: #fff;
            font-weight: 500;
        }

        #footertop .col-md-3 {
            width: 22%;
            margin-left: 2.5%;
            float: left
        }

        @media only screen and (max-width: 767px) {
            #footertop .col-md-3 {
                width: 100%;
                float: left;
                margin-top: 10px
            }
        }

        #highlights {
            margin-top: 30px;
            margin-bottom: 20px;
        }

        .highlight-block {
            width: 30%;
            margin-right: 3.33%;
            float: left;
        }

        .high {
            font-size: 35px !important;
            text-align: center;
            color: #0e5077;
            font-family: Lato, sans-serif;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .low {
            font-size: 15px;
            text-align: center;
        }

        @media only screen and (max-width : 767px) {
            .highlight-block {
                width: 100%;
                float: none;
                margin-bottom: 20px;
            }
        }
        ul li {
    line-height: 20px !important;
}
    </style>

    @yield('styles')
</head>

<body>
    @include('layouts.header')
    @if (request()->is('/'))
        {{-- <div id="slider">
            <div class="slide-container">
                <div class="paddingfix">
                    <h1>Web Hosting in Saudi Arabia</h1>
                    <p style="font-size: 15px; margin-bottom: 10px">
                        OvoHost is Saudi's top rated <strong>Web Hosting</strong> company, providing best in class
                        website hosting services!
                        <br>
                        Our hosting plans starts for as low as SR.3,000/yr including:
                    </p>

                    <div
                        style="list-style-image: url('../ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/tick-icon.png');">
                        <ul style="margin-left: 30px;">
                            <li>Super fast servers in Riyadh, Al-Kharj & Jeddah.</li>
                            <li>Free Domain, Free SSL & Wordpress</li>
                            <li>Unlimited Diskspace & Bandwidth</li>
                            <li>24/7 chat, email & whatsapp support</li>
                        </ul>
                    </div>

                    <a href="#shared-hosting"
                        style="margin:10px 20px; font-weight: bold; color: white;font-size: 12pt;border-radius: 5px;padding: 15px 30px;display: inline-block;text-decoration: none;background-color: #f4590b;">
                        View Hosting Packages!
                    </a>
                </div>
            </div>
        </div> --}}

      <div class="container">
        <div class="cus-row site-pad">
            <div class="left-col">
                <img src="{{ asset('images/server.png') }}" alt="">
                <h1>Linux Or Windows ?</h1>
                <p>No Need to worry, we 've got both</p>
            </div>
            <div class="center-col">
                <h2>Web Hosting Pakistan: Affordabale, Reliable, Secure</h2>
                <p> <i class="fa fa-square"></i> 24x7x365 Phone, Email and Chat Support</p>
                <p><i class="fa fa-square"></i>Top Web Hosting in Pakistan, with 50,000+ hosted sites</p>
                <p><i class="fa fa-square"></i>Free .com, .net, .org, .info domains with hosting package</p>
                <p><i class="fa fa-square"></i>Free Site Builder And both ASP.Net PHP Support</p>
                <p><i class="fa fa-square"></i>30 Days Moneyback Guarantee</p>
                <p><i class="fa fa-square"></i>Quick & Easy Payment Options:</p>
            </div>
            <div class="right-col">
                <div class="header">
                    <h6>Our Recent Clients</h6>
                </div>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
                <a href="">http://ustb.edu.pk/</a>
            </div>
    </div>
      </div>
    @elseif (request()->is('account/*'))
        <section id="main-menu">
            <nav id="nav" class="navbar navbar-default navbar-main" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="primary-nav">

                        <ul class="nav navbar-nav">

                            <li menuItemName="Home" class="" id="Primary_Navbar-Home">
                                <a href="index.html">
                                    Home
                                </a>
                            </li>
                            <li menuItemName="Store" class="dropdown" id="Primary_Navbar-Store">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    Store
                                    &nbsp;<b class="caret"></b> </a>
                                <ul class="dropdown-menu">
                                    <li menuItemName="Browse Products Services"
                                        id="Primary_Navbar-Store-Browse_Products_Services">
                                        <a href="indexb494.html?rp=/store">
                                            Browse All
                                        </a>
                                    </li>
                                    <li menuItemName="Shop Divider 1" class="nav-divider"
                                        id="Primary_Navbar-Store-Shop_Divider_1">
                                        <a href="#">
                                            -----
                                        </a>
                                    </li>
                                    <li menuItemName="Web Hosting" id="Primary_Navbar-Store-Web_Hosting">
                                        <a href="indexb494.html?rp=/store/web-hosting">
                                            Web Hosting
                                        </a>
                                    </li>
                                    <li menuItemName="Reseller Hosting" id="Primary_Navbar-Store-Reseller_Hosting">
                                        <a href="index93fa.html?rp=/store/reseller-hosting">
                                            Reseller Hosting
                                        </a>
                                    </li>
                                    <li menuItemName="Web Development" id="Primary_Navbar-Store-Web_Development">
                                        <a href="index9f47.html?rp=/store/web-development">
                                            Web Development
                                        </a>
                                    </li>
                                    <li menuItemName="Other" id="Primary_Navbar-Store-Other">
                                        <a href="indexc21e.html?rp=/store/other">
                                            Other
                                        </a>
                                    </li>
                                    <li menuItemName="Hosting &amp; .PK Domain"
                                        id="Primary_Navbar-Store-Hosting_&amp;_.PK_Domain">
                                        <a href="indexc48e.html?rp=/store/hosting-and-pk-domain">
                                            Hosting &amp; .PK Domain
                                        </a>
                                    </li>
                                    <li menuItemName="Windows Hosting" id="Primary_Navbar-Store-Windows_Hosting">
                                        <a href="index9628.html?rp=/store/windows-hosting">
                                            Windows Hosting
                                        </a>
                                    </li>
                                    <li menuItemName="Business Hosting" id="Primary_Navbar-Store-Business_Hosting">
                                        <a href="index388e.html?rp=/store/business-hosting">
                                            Business Hosting
                                        </a>
                                    </li>
                                    <li menuItemName="SSD Hosting" id="Primary_Navbar-Store-SSD_Hosting">
                                        <a href="indexb84a.html?rp=/store/ssd-hosting">
                                            SSD Hosting
                                        </a>
                                    </li>
                                    <li menuItemName="Register a New Domain"
                                        id="Primary_Navbar-Store-Register_a_New_Domain">
                                        <a href="cart2029.html?a=add&amp;domain=register">
                                            Register a New Domain
                                        </a>
                                    </li>
                                    <li menuItemName="Transfer a Domain to Us"
                                        id="Primary_Navbar-Store-Transfer_a_Domain_to_Us">
                                        <a href="cart7c76.html?a=add&amp;domain=transfer">
                                            Transfer Domains to Us
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li menuItemName="Announcements" class="" id="Primary_Navbar-Announcements">
                                <a href="index992c.html?rp=/announcements">
                                    Announcements
                                </a>
                            </li>
                            <li menuItemName="Knowledgebase" class="" id="Primary_Navbar-Knowledgebase">
                                <a href="indexded0.html?rp=/knowledgebase">
                                    Knowledgebase
                                </a>
                            </li>
                            <li menuItemName="Network Status" class="" id="Primary_Navbar-Network_Status">
                                <a href="index804a.html">
                                    Network Status
                                </a>
                            </li>
                            <li menuItemName="Affiliates" class="" id="Primary_Navbar-Affiliates">
                                <a href="index804a.html">
                                    Affiliates
                                </a>
                            </li>
                            <li menuItemName="Contact Us" class="" id="Primary_Navbar-Contact_Us">
                                <a href="submitticket74c3.html">
                                    Contact Us
                                </a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li menuItemName="Account" class="dropdown" id="Secondary_Navbar-Account">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    Account
                                    &nbsp;<b class="caret"></b> </a>
                                <ul class="dropdown-menu">
                                    <li menuItemName="Login" id="Secondary_Navbar-Account-Login">
                                        <a href="index804a.html">
                                            Login
                                        </a>
                                    </li>
                                    <li menuItemName="Register" id="Secondary_Navbar-Account-Register">
                                        <a href="register.html">
                                            Register
                                        </a>
                                    </li>
                                    <li menuItemName="Divider" class="nav-divider"
                                        id="Secondary_Navbar-Account-Divider">
                                        <a href="#">
                                            -----
                                        </a>
                                    </li>
                                    <li menuItemName="Forgot Password?" id="Secondary_Navbar-Account-Forgot_Password?">
                                        <a href="index0839.html?rp=/password/reset">
                                            Forgot Password?
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </nav>
        </section>
    @endif

    <div class="container" style="background: #fff">
        @yield('content')

        @if (!request()->is('account/*'))
            @include('layouts.footertop')
        @endif
        <div class="clear"></div>
    </div>
    @if (request()->is('account/*'))
        @include('layouts.footertop')
    @endif
    @include('layouts.footer')
</body>
@yield('scripts')

</html>
