@if (!request()->is('account/*'))
    <div id="customers">
        <h3>Our customers are our top priority!</h3>
        <p>OvoHost proudly ofers web hosting & domain services to 1000s of startups, bloggers, e-commerce
            platform
            SMEs in
            Saudi Arabia since 2009.
            Here is a quick overview of the services we delivered this year:</p>

        <div id="highlights">
            <div class="highlight-block">
                <p class="high">5+ Million</p>
                <p class="low">Pages Served</p>
            </div>

            <div class="highlight-block">
                <p class="high">1+ Million</p>
                <p class="low">Emails Sent!</p>
            </div>

            <div class="highlight-block">
                <p class="high">~99.91%</p>
                <p class="low">Uptime Recorded!</p>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
        <p style="text-align: center; margin-top: 20px"><img src="{{ asset('images/feedback.png') }}"
                alt="feedback"><br>
            <i>I moved to
            ovohost.com from other
                Saudi Arabia hosting company 3 months ago, their webhosting is amazing and support is unbeatable,
                highly
                recommended!</i> - <strong>CEO - Saleshub</strong><br> <i>OvoHost activated my account within
                10
                minutes as
                committed, transferred my data and helped me with website security. I made right decision to
                select
                ovohost.com</i> - <strong>Atta Khan</strong>
        </p>
        <p style="text-align:center"><a href="https://whmedium.com/company/easyhost.com" target="_blank">OvoHost's
                reviews
                at WHmedium</a></p>

    </div>
@endif
<div id="footertop">
    <div class="col-md-3">
        <figure><img src="{{ asset('images/WhatsApp_Footer.png') }}" alt="Image"></figure>
        <dl>
            <dt>WhatsApp</dt>
            <dd>+966 59 478 8122</dd>
           
        </dl>
    </div>
    <div class="col-md-3">
        <figure><img src="{{ asset('images/email_Footer.png') }}" alt="Image"></figure>
        <dl>
            <dt>Email</dt>
            <dd>info@ovohost.com</dd>
            <dd>support@ovohost.com</dd>
        </dl>
    </div>
    <div class="col-md-3">
        <figure><img src="{{ asset('images/Phone_Footer.png') }}" alt="Image"></figure>
        <dl>
            <dt>Sales</dt>
            <dd>+966 59 478 8122</dd>
            
        </dl>
    </div>
    <div class="col-md-3">
        <figure><img src="{{ asset('images/Working-Hours_Footer.png') }}" alt="Image"></figure>
        <dl>
            <dt>Working hours</dt>
            <dd>Support: Ticket / Email</dd>
            <dd>Call: 9am - 7pm</dd>
        </dl>
    </div>
</div>
