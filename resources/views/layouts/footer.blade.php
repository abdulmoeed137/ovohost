<div id="footer">
    <div class="container" style="background:#fff">
        <div class="content-adj">
            <div class="ft_rep"><span class="ft_heading">Hosting</span>
                <ul>
                    <li><a href="{{ route('host', 'web') }}" class="ft_link">Web Hosting</a></li>
                    <li><a href="{{ route('host', 'unlimited') }}" class="ft_link">Unlimited Hosting</a></li>
                    <li><a href="{{ route('host', 'ssd') }}" class="ft_link">SSD Hosting</a></li>
                    <li><a href="{{ route('host', 'business') }}" class="ft_link">Business Hosting</a></li>
                    <li><a href="{{ route('host', 'pk-domain') }}" class="ft_link">Hosting with Free .COM</a></li>
                    <li><a href="{{ route('host', 'reseller') }}" class="ft_link">Reseller Hosting</a></li>
                </ul>
            </div>
            <div class="ft_rep"><span class="ft_heading">Services</span>
                <ul>
                    <li><a href="{{route('domain')}}" class="ft_link">Domains</a></li>
                    <li><a href="{{route('pkdomain')}}" class="ft_link">PK Domains</a></li>
                    <li><a href="{{route('pkprepaidcard')}}" class="ft_link">PKNIC Prepaid Cards</a></li>
                    <li><a href="{{route('webdevelopment')}}" class="ft_link">Web Development</a></li>
                    <li><a href="{{route('seo')}}" class="ft_link">SEO</a></li>
                    <li><a href="{{route('services')}}" class="ft_link">Services</a></li>
                </ul>
            </div>
            <div class="ft_rep"><span class="ft_heading">Important</span>
                <ul>
                    <li><a href="{{ route('payment') }}">Payment Options</a></li>
                    <li><a href="{{route('affiliates')}}">Affiliates</a></li>
                    <li><a href="https://blog.easyhost.pk/">Blog</a></li>
                    <li><a href="{{route('whoisdomain')}}">Domain Whois</a></li>
                    <li><a href="{{route('faq')}}">FAQs</a></li>
                    <li><a href="{{route('career')}}">Careers</a></li>
                </ul>
            </div>
            <div class="ft_rep" style="width:25%">
                <span class="ft_heading">Subscribe Us</span>
                <p id='subscribe' style='margin-bottom: 10px'>Submit email to get the special discounts, be the first to
                    know our deals:</p>
                <form method="post" action="#subscribe">
                    <input name="field_0" type="email" placeholder="yourname@gmail.com"
                        style="padding:5px 2px; font-weight: bold; margin-bottom: 10px; width: 70%">
                    <input type="submit" name="submitBtn" value="Subscribe"
                        style="padding: 5px 0px; font-weight: bold; width: 25%">
                </form>
            </div>
            <p style="text-align: center">
                <a href="new-year-sale80cc.html?source=footer">New Year Sale</a> |
                <a href="tos.html">Terms Of Services</a> |
                <a href="tos.html#money-back-policy">Money Back Policy</a> |
                <a href="tos.html#privacy-policy">Privacy Policy</a> |
                <a href="discounts.html">Discounts</a> |
                <a href="sitemap.html">Sitemap</a>
            </p>
            <p style="text-align: center">Copyright &copy; 2016-2020 OvoHost. All Rights Reserved.</p>
        </div>
        @if (request()->is('/'))
            <div class="clear"></div>
            <div style="background:#f1f1f1; padding:20px 10px;">
                <h3 style="font-size:14px; line-height:20px">Domain Name and Hosting</h3>
                <p style="font-size:13px; line-height:18px">At OvoHost, you can select from the wide range to top class
                    Domain Name and Hosting packages for your website starting for as low as SR.2,200/yr including free
                    domain and SSL. Looking for reliable Web Hosting in Saudi Arabia? look no further, OvoHost has
                    everything
                    you will need to get your webpage online within 15 minutes.</p>
                <h3 style="font-size:14px; line-height:20px; margin-top:10px">Com Domain Name Registration</h3>
                <p style="font-size:13px; line-height:18px">OvoHost is the leading .Com domain name registration service
                    provider in Saudi Arabia offering the cheapest .Com rates (SR.2,350/2yr) and the fastest registration
                    time in
                    industry. All .Com domains registered with us are activated within 15 minutes and customer has 100%
                    ownership.</p>
                <h3 style="font-size:14px; line-height:20px; margin-top:10px">OvoHost Reviews & Comparison vs Other
                    Providers</h3>
                <p style="font-size:13px; line-height:18px">Ovohost has been in the industry since 2009 and have service
                    1000s of customers. Unlike other web hosting companies in Saudi Arabia, OvoHost owns servers and ensure
                    the
                    highest quality. You can <a href="https://whmedium.com/company/easyhost.pk" target="_blank">find
                    OvoHost's reviews at WHmedium</a></p>
            </div>
        @endif
    </div>
</div>
