<style>
   ul{
        padding: 0;
        list-style: none;
    }
    ul li{
        display: inline-block;
        position: relative;
        line-height: 50px;
        text-align: left;
    }
    ul li a{
        display: block;
        padding: 8px 25px;
        color: #333;
        text-decoration: none;
    }
    ul li a:hover{
        color: #fff;
        background: #939393;
    }
    ul li ul.dropdown{
        min-width: 100%; /* Set width of the dropdown */
        background: #f2f2f2;
        display: none;
        position: absolute;
        z-index: 999;
        left: 0;
    }
    ul li:hover ul.dropdown{
        display: block;	/* Display the dropdown */
      
      padding:0px;
    }
    ul li ul.dropdown li{
        display: block;
    }
</style>
<!--<style>
    @media all and (min-width: 992px) {
	.nav-item .dropdown-menu{ display: none; }
	.nav-item:hover .nav-link{   }
	.nav-item:hover .dropdown-menu{ display: block; }
	.nav-item .dropdown-menu{ margin-top:0; }
}
</style> -->
<div id="notification">
    <div class="container">
        <div class="not-right">
            <ul>
                <li>
                    <p style="padding-top: 7px;">24x7 Sales: +966 59 478 8122</p>
                </li>
                <li><a href="{{ route('account.register') }}">Register</a></li>
                <li><a href="{{ route('account.login') }}">Login</a></li>
                <li><a href="account/index804a.html">Support</a></li>
                <li><a href="{{ route('contact') }}">Contact Us</a></li>
            </ul>
        </div>
        <div class="not-left">
            <p style="padding-top: 7px;">Saudi Arabia's #1 affordable Web Hosting & Com Domain provider.</p>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="header">
    <div class="container" style="background: #fff">
        <div id="logo">
            <a href="{{ route('home') }}">
                <img width="150" src="{{ asset('images/logo-sm.png') }}" alt="ovohost.pk"/>
            </a>
        </div>

        <div id="top-area">
            <div id="menu-icon" style="cursor:pointer" onclick="openNav()">&nbsp;&#9776;&nbsp;</div>
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <a href="{{ route('home') }}">Home</a>
                <a href="{{ route('host', 'web') }}">Web Hosting</a>
                <a href="{{ route('host', 'unlimited') }}">Unlimited Hosting
                    <sup
                        style="font-size: 12px; padding: 1px 5px; background-color:#2fb145; color: white; border-radius: 2px">HOT</sup>
                </a>
                <a href="{{ route('host', 'pk-domain') }}">.COM + Hosting</a>
                <a href="{{ route('host', 'ssd') }}">SSD Hosting</a>
                <a href="reseller-hosting.html">Reseller Hosting</a>
                <a href="{{ route('domain') }}">Domains</a>
                <a href="pk-domains.html">COM Domains</a>
                <a href="{{ route('payment') }}">Payments</a>
                <a href="contact.html">Contact Us</a>
                <a href="account/index804a.html">Client Login</a>
                <a href="account/register.html">Register</a>
            </div>

            <script>
                function openNav() {
                    document.getElementById("mySidenav").style.width = "200px";
                    document.getElementById("menu-icon").style.marginLeft = "0";
                }

                function closeNav() {
                    document.getElementById("mySidenav").style.width = "0";
                    document.getElementById("menu-icon").style.marginLeft = "0";
                }
            </script>

            <div id="menu">
                <div>
                    <ul id="menu-main" class="menu">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <!-- <li><a href="{{ route('host', 'web') }}">Hosting</a></li> -->
                        <!-- <li class="nav-item dropdown">
                                <a class="nav-link  dropdown-toggle" href="#" data-bs-toggle="dropdown">  Hover me  </a>
                                    <ul class="dropdown-menu">
                                    <ol><a class="dropdown-item" href="#"> Submenu item 1</a></ol>
                                    <ol><a class="dropdown-item" href="#"> Submenu item 2 </a></ol>
                                    <ol><a class="dropdown-item" href="#"> Submenu item 3 </a></ol>
                                    </ul>
                        </li> -->
                        <li>
                            <a href="{{ route('hosting') }}">Hosting </a>
                            <ul class="dropdown">
                                {{-- <li><a href="{{ route('host', 'web') }}">Web Hosting</a></li>
                                <li><a href="{{ route('host', 'unlimited') }}">Unlimited Hosting</a></li> --}}
                                <!-- <li><a href="#">Printers</a></li> -->
                            </ul>
                        </li>
                        <li><a href="{{ route('host', 'unlimited') }}">Unlimited Hosting <sup style="font-size: 12px; padding: 1px 5px; background-color:#2fb145; color: white; border-radius: 2px">HOT</sup></a>
                        </li>
                        <!-- <li><a href="{{ route('host', 'pk-domain') }}">.COM + Hosting</a></li> -->
                        <!-- <li><a href="{{ route('host', 'ssd') }}">SSD</a></li> -->
                        <li><a href="{{ route('domain') }}">Domains</a></li>
                        <li><a href="{{ route('payment') }}">Payments</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
