@extends('layouts.app')

@section('content')
    <div id="domain-checker">
        <p class="left" style="margin-left: 30px; font-weight:bold">Search domain: &nbsp;&nbsp;</p>
        <form action="https://www.easyhost.pk/account/cart.php?a=add&amp;domain=register" method="post"
            class="left">
            <input type="text" class="dinput" placeholder="your domain name" name="sld" size="20">
            <select class="dinput" name="tld">
                <option>.com</option>
                <option>.net</option>
                <option>.org</option>
                <option>.info</option>
                <option>.co</option>
                <option>.pk</option>
                <option>.edu.pk</option>
                <option>.org.pk</option>
                <option>.com.pk</option>
                <option>.ca</option>
                <option>.co.uk</option>
                <option>.net.pk</option>
                <option>.us</option>
                <option>.biz</option>
                <option>.tv</option>
            </select>
            <button class="dinput">Search Domain</button>
        </form>
        <p class="left" style="margin-left: 10px">.COM @ SR.1,700/yr, .NET @ SR.2,400/yr, .COM @ SR.2,350/2yrs!</p>
        <div class="clear"></div>
    </div>
    <div class="content-adj">

        <div class="tab">
            <button class="tablinks" onclick="openCity(event, 'cat1')" id="defaultOpen">Linux Web Hosting
                Packages</button>
            <button class="tablinks" onclick="openCity(event, 'cat2')">Windows Web Hosting Packages</button>
        </div>

        @foreach ($categories as $c => $category)
            <div id="cat{{ $c }}" class="tabcontent">
                <div id="packages">
                    @foreach ($category as $key1 => $package)
                        <div class="pricebox{{ $key1 == 0 ? ' pricebox-a' : '' }}"
                            {{ $package->is_premium ? 'style=border-color:#f4590b!important' : '' }}>
                            <form id="package-{{ $c . $key1 }}" method="GET"
                                action="{{ $package->durations[0]->link }}">
                                <div class="pricehead"
                                    {{ $package->is_premium ? 'style=background:#f4590b!important' : '' }}>
                                    {{ $package->title }}</div>
                                <div class="amount">
                                    SAR {{ $package->durations[0]->price }}/yr
                                    <select name="billingcycle" class="price-option"
                                        onchange="onDurationChange({{ $c . $key1 }}, event)">
                                        @foreach ($package->durations as $key2 => $duration)
                                            <option value="{{ $key2 }}" data-link="{{ $duration->link }}"
                                                data-domain="{{ $duration->domain }}">
                                                {{ "$duration->no {$periods[$duration->period]} - SR. $duration->price Save $duration->save%" }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <ul class="align-ul">
                                    @foreach ($package->features as $key3 => $feature)
                                        <li {{ $feature->class ? 'class=' . $feature->class : '' }}>
                                            {{ $feature->text }}
                                        </li>
                                        @if ($key3 + 1 == 1 && $package->durations[0]->domain)
                                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                                        @endif
                                    @endforeach
                                    <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                                </ul>
                                <p style="text-align: center; margin-bottom: 10px">
                                    <button class="order-b-form" type="submit">Order Now!</button>
                                    <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=1" class="order-b">Order Now!</a>-->
                                </p>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach

        <div id="London" class="tabcontent">
            <div id="packages">
                <div class="pricebox pricebox-a">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget I</div>
                        <div class="amount">
                            SAR 3,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="1" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR. 3,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.5,400 Save 10%</option>
                                <option value="triennially">3 Years - SR.7,650 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">1500 MB Diskspace</li>
                            <li>25 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 1 Domain</strong></li>
                            <li>50 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 4,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=1" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget II</div>
                        <div class="amount">
                            SAR 3,750/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="2" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.3,750 Save 0%</option>
                                <option value="biennially">2 Years - SR.6,750 Save 10%</option>
                                <option value="triennially">3 Years - SR.9,500 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">2500 MB Diskspace</li>
                            <li>50 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 2 Domains</strong></li>
                            <li>100 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 5,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=2" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget III</div>
                        <div class="amount">
                            SAR 4,500/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="5" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.4,500 Save 0%</option>
                                <option value="biennially">2 Years - SR.8,100 Save 10%</option>
                                <option value="triennially">3 Years - SR.11,500 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">3500 MB Diskspace</li>
                            <li>75 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 3 Domains</strong></li>
                            <li>150 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 6,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=5" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox" style="border-color:#f4590b !important">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead" style="background:#f4590b !important">Unlimited I</div>
                        <div class="amount">
                            SAR 6,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="3" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.6,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.10,800 Save 10%</option>
                                <option value="triennially"> 3 Years - SR.15,300 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">Unlimited Diskspace</li>
                            <li>Unlimited Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 1 Domain</strong></li>
                            <li>Unlimited Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 7,500/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox" style="border-color:#f4590b !important">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead" style="background:#f4590b !important">Unlimited II</div>
                        <div class="amount">
                            SAR 9,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="4" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.9,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.16,200 Save 10%</option>
                                <option value="triennially"> 3 Years - SR.22,900 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">Unlimited Diskspace</li>
                            <li>Unlimited Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 10 Domains</strong></li>
                            <li>Unlimited Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 10,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

            </div>
        </div>

        <div id="Paris" class="tabcontent">
            <div id="packages">
                <div class="pricebox pricebox-a">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget I</div>
                        <div class="amount">
                            SAR 3,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="1" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR. 3,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.5,400 Save 10%</option>
                                <option value="triennially">3 Years - SR.7,650 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">1500 MB Diskspace</li>
                            <li>25 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 1 Domain</strong></li>
                            <li>50 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 4,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=1" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget II</div>
                        <div class="amount">
                            SAR 3,750/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="2" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.3,750 Save 0%</option>
                                <option value="biennially">2 Years - SR.6,750 Save 10%</option>
                                <option value="triennially">3 Years - SR.9,500 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">2500 MB Diskspace</li>
                            <li>50 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 2 Domains</strong></li>
                            <li>100 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 5,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=2" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead">Budget III</div>
                        <div class="amount">
                            SAR 4,500/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="5" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.4,500 Save 0%</option>
                                <option value="biennially">2 Years - SR.8,100 Save 10%</option>
                                <option value="triennially">3 Years - SR.11,500 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">3500 MB Diskspace</li>
                            <li>75 GB Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 3 Domains</strong></li>
                            <li>150 Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 6,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=5" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox" style="border-color:#f4590b !important">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead" style="background:#f4590b !important">Unlimited I</div>
                        <div class="amount">
                            SAR 6,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="3" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.6,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.10,800 Save 10%</option>
                                <option value="triennially"> 3 Years - SR.15,300 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">Unlimited Diskspace</li>
                            <li>Unlimited Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 1 Domain</strong></li>
                            <li>Unlimited Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 7,500/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=3" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

                <div class="pricebox" style="border-color:#f4590b !important">
                    <form action="https://www.easyhost.pk/account/cart.php">
                        <div class="pricehead" style="background:#f4590b !important">Unlimited II</div>
                        <div class="amount">
                            SAR 9,000/yr
                            <input type="hidden" value="add" name="a">
                            <input type="hidden" value="4" name="pid">
                            <input type="hidden" value="1" name="currency">
                            <select name="billingcycle" class="price-option">
                                <option value="annually">1 Year - SR.9,000 Save 0%</option>
                                <option value="biennially">2 Years - SR.16,200 Save 10%</option>
                                <option value="triennially"> 3 Years - SR.22,900 Save 15%</option>
                            </select>
                            <input type="hidden" value="homepage" name="source">
                        </div>
                        <ul class="align-ul">
                            <li class="boldi">Unlimited Diskspace</li>
                            <li>Unlimited Bandwidth</li>
                            <li class="green">Free Domain Registration<br>(.com domain)</li>
                            <li class="green">SSD Storage (3x Faster)</li>
                            <li class="green">Free SSL Certificate</li>
                            <li><strong>Host 10 Domains</strong></li>
                            <li>Unlimited Email Accounts</li>
                            <li>cPanel, PHP, MySQL</li>
                            <li>99.9% Uptime, 24x7 Helpline</li>
                            <li>Free Website Builder</li>
                            <li class="line">Regular Price: SAR 10,000/yr</li>
                            <li><a href="{{ route('host', 'web') }}">More Details</a></li>
                        </ul>
                        <p style="text-align: center; margin-bottom: 10px">
                            <button class="order-b-form" type="submit">Order Now!</button>
                            <!--<a href="https://www.easyhost.pk/account/cart.php?a=add&pid=4" class="order-b">Order Now!</a>-->
                        </p>
                    </form>
                </div>

            </div>
        </div>
        <p></p>



        <div class="clear"></div>

        <section
            style="padding-top:10px; padding-bottom:10px; margin-top:20px; border:1px solid #000; border-radius:10px; text-align:center">

            <h3
                style="text-align: center; padding-bottom: 5px; margin-bottom: 5px; font-size: 20px; color:darkgreen; border-bottom:1px solid #000">
                Latest news
            </h3>

            <p style="padding-left:20px; padding-right:20px">
                OvoHost has launched Knowledgebase & Announcement board for it's customers at <a target="_blank"
                    href="https://easyhost.wiki.pk/?source=easyhost.pk">ovohost.com</a>
            </p>
            <p style="margin-top:10px">
                <a target="_blank"
                    style="background:darkgreen; color:white; border:0; padding:5px 10px; text-decoration:none;"
                    href="https://easyhost.wiki.pk/?source=easyhost.pk">Visit Knowledgebase</a>
            </p>

        </section>

        <div class="clear"></div>

        <section class="feature-block">
            <h3 style="text-align: center; margin-bottom: 15px; font-size: 20px">
                Our web hosting plans include 100+ free scripts for you. Some open source scripts that our clients
                love:
            </h3>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/wordpress.png" alt="wordpress" />
                <h4>Wordpress</h4>
                <p>World's leading framework for Blogs & Corporate Websites.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/joomla.png" alt="joomla" />
                <h4>Joomla</h4>
                <p>PHP framework for creating websites with easy to us admin portal.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/drupal.png" alt="drupal" />
                <h4>Drupal</h4>
                <p>PHP based opensource CMS for creating websites and web applications.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/smf.png" alt="SMF Forum" />
                <h4>SMF Forums</h4>
                <p>SMF Forums help you start your own forums in just few clicks.</p>
            </div>

            <div class="clear"></div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/opencart.png" alt="Opencart" />
                <h4>OpenCart</h4>
                <p>Fastest way to take your store online live today in few clicks.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/prestashop.png" alt="prestashop" />
                <h4>PrestaShop</h4>
                <p>Popular platform to make an online shopping website for your products.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/magento.png" alt="magento" />
                <h4>Magento</h4>
                <p>Popular for its eCommerce functionality, security and scalability.</p>
            </div>

            <div class="qtr-service">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/osCommerce.png" alt="osCommerce" />
                <h4>osCommerce</h4>
                <p>Another eCommerce tool to make your shop get online on the website.</p>
            </div>

            <div class="clear"></div>
        </section>

        <div class="clear"></div>

        <h3 style="text-align: center; margin:40px 0px 10px 0px; font-size: 30px">All hosting options in one place
        </h3>

        <div class="col_one_third">
            <div class="padding10">
                <h3>Budget Hosting</h3>
                <p style="font-size: 16px;">Buy easy and affordable web hosting and get started in just 30 minutes.
                </p>
                <p><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                        data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/icon-hosting.png"
                        alt="Web hosting"></p>
                <p><span style="font-size: 16x; color: #e68e35;"><strong>Starting from SR.3,000/yr</strong></span>
                </p>
                <p><a href="{{ route('host', 'web') }}" title="web hosting">See All Plans</a></p>
            </div>
        </div>

        <div class="col_one_third">
            <div class="padding10">
                <h3>Unlimited Hosting</h3>
                <p style="font-size: 16px;">Get Unlimited Hosting to kick start your big website ideas without
                    limits.</p>
                <p><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                        data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/icon-business.png"
                        alt="Unlimited Hosting"></p>
                <p><span style="font-size: 16x; color: #e68e35;"><strong>Starting from SR.6,000/yr</strong></span>
                </p>
                <p><a href="{{ route('host', 'unlimited') }}" title="unlimited hosting">See All Plans</a></p>
            </div>
        </div>

        <div class="col_one_third">
            <div class="padding10">
                <h3>Saudi Arabia Hosting</h3>
                <p style="font-size: 16px;">Start your web hosting business using our Reseller Hosting option.</p>
                <p><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                        data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/icon-hosting.png"
                        alt="Reseller Hosting"></p>
                <p><span style="font-size: 16x; color: #e68e35;"><strong>Starting from SR.8,000/yr</strong></span>
                </p>
                <p><a href="{{ route('host', 'pk-domain') }}" title="reseller hosting">See All Plans</a></p>
            </div>
        </div>

        <div class="clear"></div>

        <div class="services" style="margin-top: 30px">
            <div class="services_left">
                <div class="services_padding">
                    <h3>#1 Saudi Arabia Domain Registration</h3>
                    <p>
                        Need .COM domain for your website? Get .COM registered for the cheapest rates in Saudi Arabia!
                        Search your domain name with top rates .COM provider in Saudi Arabia.
                    </p>
                    <ul>
                        <li>Cheapest .COM domain rates</li>
                        <li>Saudi Arabia's top rated .COM provider</li>
                        <li>Registration within 15 minutes</li>
                        <li>100% ownership of your .COM</li>
                    </ul>

                    <div class="clear"></div>
                    <div style="margin-top:30px">
                        <a style="padding: 10px" href="pk-domains8606.html?ref=banner">Search .COM domain now!</a>
                    </div>
                </div>
            </div>

            <div class="services_right">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                    data-src="https://ehfiles.sgp1.cdn.digitaloceanspaces.com/estatic/pk-domain-com.png"
                    alt="pk domain registration">
            </div>

            <div class="clear"></div>
        </div>

        <h3 style="margin:40px 0px 10px 0px; font-size: 25px; padding:0px 20px">Why choose OvoHost.com?</h3>
        <p style="line-height: 25px; padding:0px 20px">OvoHost.com is a fast growing <strong>web hosting company in
                Saudi Arabia</strong>, offering services since 2009 with experience of managing 10,000+ websites. Our
            services include cheap <strong>.COM Domain registration</strong>, reliable <strong>website
                hosting</strong> and web development.<br><br>
            Our hosting comes with hundreds of features including free domain, free site builder, rock solid 99.9%
            uptime, free domain name & 30 days money back guarantee - so you can have peace of mind! We have clients
            for <a href="web-hosting.html">Web Hosting in Riyadh</a>, Al-Kharj, Jeddah and all other major cities
            in Saudi Arabia, give us a try now!<br><br>
            We at OvoHost ensure that we deliver the highest quality hosting service with the lowest possible price
            to our customers - this means you get high standard cPanel hosting at a much lower price compared to
            martket! Our hosting plans are designed for individuals and small businesses to host their websites and
            not worry about spending huge cost for setup.<br><br>
            If you are looking for <strong>Domain Registration and Website Hosting service</strong>, you have just
            gotten to right place!
        </p>
        <br>
        <div style="margin:5px 20px;">
            <div style="margin:0px auto; max-width:146px">
                <div class="fb-like" data-href="https://www.easyhost.pk/" data-layout="button_count"
                    data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
            </div>
        </div>
        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #f4590b" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM
                US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>

        <div class="clear"></div>
    </div>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

        function onDurationChange(p, e) {
            const formEl = document.getElementById(`package-${p}`)
            const ulEl = formEl.children[2]
            const txt = "Free Domain Registration<br>(.com domain)"
            const hasDomain = e.target.options[e.target.selectedIndex].dataset.domain == 1
            if (ulEl.children[1].getInnerHTML() == txt && !hasDomain) {
                ulEl.children[1].remove()
            } else if (ulEl.children[1].getInnerHTML() != txt && hasDomain) {
                ulEl.children[0].insertAdjacentHTML("afterEnd", `<li class="green">${txt}</td>`);
            }
            formEl.action = e.target.options[e.target.selectedIndex].dataset.link
        }
    </script>
@endsection
