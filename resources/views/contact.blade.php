@extends('layouts.app')

@section('title', 'Contact Us | OvoHost')

@section('styles')
    <style>
        #con-tab {
            margin-top: 20px;
            margin-bottom: 20px;
            background: #f1f1f1;
            border: #b9b9b9 1px solid
        }

        .con-tab-one {
            width: 33.334%;
            float: left;
            line-height: 30px;
            text-align: center;
            margin-bottom: 20px
        }

        .con-tab-one h4 {
            background-color: #0e5077;
            color: white;
            margin-bottom: 15px
        }

        @media only screen and (max-width : 767px) {
            .con-tab-one {
                width: 100%
            }
        }

        #customers {
            width: 100%;
            background-color: #f3f3f3;
            margin-top: 20px;
            padding-bottom: 20px
        }

        #customers h3 {
            padding: 30px 50px 10px 50px;
            font-size: 25px
        }

        #customers p {
            padding: 0px 50px;
            font-size: 17px;
            padding-bottom: 10px
        }

        #customers ul {
            margin: 0px 50px
        }

        #customers ul li {
            list-style: none;
            float: left;
            font-size: 16px;
            line-height: 30px;
            width: 220px
        }

        #customers ul li a {
            color: blue
        }

        #highlights {
            margin-top: 30px;
            margin-bottom: 20px;
        }

        .highlight-block {
            width: 30%;
            margin-right: 3.33%;
            float: left;
        }

        .high {
            font-size: 35px !important;
            text-align: center;
            color: #0e5077;
            font-family: Lato, sans-serif;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .low {
            font-size: 15px;
            text-align: center;
        }

        @media only screen and (max-width : 767px) {
            .highlight-block {
                width: 100%;
                float: none;
                margin-bottom: 20px;
            }
        }

        #footertop {
            width: 100%;
            float: left;
            padding-top: 20px;
            padding-bottom: 10px
        }

        #footertop figure {
            float: left;
            margin-right: 20px;
            margin-top: 10px;
            margin-bottom: 20px;
        }

        #footertop dl {
            display: block;
        }

        #footertop dl dt {
            font-size: 18px;
            font-weight: 700;
            color: #fff;
            margin-bottom: 10px;
        }

        #footertop dl dd {
            color: #fff;
            font-weight: 500;
        }

        #footertop .col-md-3 {
            width: 22%;
            margin-left: 2.5%;
            float: left
        }

        @media only screen and (max-width: 767px) {
            #footertop .col-md-3 {
                width: 100%;
                float: left;
                margin-top: 10px
            }
        }

    </style>
@endsection

@section('content')

    <div id="headline">
        <h1>Contact Us</h1>
    </div>

    <div class="content-adj">
        <p>We are available for help - please feel free to contact us:</p>
        <div id="con-tab">
            <div class="con-tab-one">
                <h4>Phone / Whatsapp Sales</h4>
                <p><strong>+966 59 478 8122</strong></p>
               
            </div>

            <div class="con-tab-one">
                <h4>Departments</h4>
                <p><a href="account/index804a.html?step=2&amp;deptid=1">Support Department</a></p>


                <p><a href="account/submitticket74c3.html?step=2&amp;deptid=2">Sales Department</a></p>


                <p><a href="account/submitticket30a4.html?step=2&amp;deptid=3">General Queries</a></p>


                <p><a href="account/submitticketd850.html?step=2&amp;deptid=4">Complaints</a></p>

            </div>

            <div class="con-tab-one">
                <h4>Emails</h4>
                <p><a href="mailto:info@easyhost.pk">info@ovohost.com</a></p>
                <p><a href="mailto:sales@easyhost.pk">sales@ovohost.com</a></p>
                <p><a href="mailto:support@easyhost.pk">support@ovohost.com</a><br><i>(for customers only)</i></p>

            </div>

            <div class="clear"></div>

        </div>

        <p>You can also contact us via Live Chat through the website - however for technical support we suggest you to open
            a support ticket for fast resolution to your queries.</p>


        <div class="buttonset">
            <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
            <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM
                US</a>
            <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
        </div>

        <div class="clear"></div>

        <div class="clear"></div>
    </div>
@endsection
