@extends('layouts.app')

@section('title', 'Submit Ticket - OvoHost')



@section('content')

<div id="headline">
<h1>SEO</h1>
        </div>

        <div class="content-adj">
            <h3 style="font-size: 40px; margin-bottom: 10px">Search Engine Optimization</h3>

            <p style="line-height: 30px">

            OvoHost provides SEO services to on-shore and off-shore customers at affordable prices. Our services
                include:

            </p>
            <ul style="line-height: 25px; margin-bottom: 10px; margin-left: 20px">
                <li>On-site optimization</li>
                <li>Backlinks building (high quality)</li>
                <li>Competitive keyword analytics</li>
                <li>SEO friendly content writing</li>
                <li>Guest blog posting</li>
                <li>Feedback write ups</li>
                <li>Directory submissions</li>
            </ul>

            <p style="line-height: 30px">
                We can do all the hard work to get your website among the top listings on Google for your related
                keywords.
                <br>
                Interested in our SEO service? Contact us with details of what you are trying to achieve and we can help
                you get there!
                <br><br>
                <a href="account/submitticket74c3.html?step=2&amp;deptid=2" class="order-btn"
                    style="padding: 15px; font-size: 15px; background-color: #1b4f6d; color: white">Contact Us</a>
            </p>


            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
