@extends('layouts.app')

@section('title', 'Careers | OvoHost')


@section('content')

<div id="headline">
            <h1>Careers</h1>
        </div>

    <div class="content-adj">
            <header class="header">
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">


                <h1 class="entry-title">Careers</h1>

            </header>


            <section class="entry-content">


                <p>We are looking for people! Available positions are given below:</p>
                <ul>
                    <li>SEO executive</li>
                    <li>Content writers (blogs)</li>
                    <li>PHP developer</li>
                    <li>Front-end developer</li>
                </ul>
                <p>If you have the right experience and are willing to work with the fast pace environment and dynamic
                    team,
                    please drop your resume at careers[@]ovohost.com (please mention job title in the subject).</p>


                <div class="entry-links"></div>


            </section>


            </article>


            <section id="comments">
            </section>


            </section>


            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{ route('AboutUs') }}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{ route('host', 'web') }}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{ route('contact') }}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
    </div>
@endsection