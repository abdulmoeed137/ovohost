@extends('layouts.app')

@section('title', 'Web Development Saudi Arabia | Software House in Jeddah | OvoHost')

@section('styles')
<style>
                .orderbtn a {
                    background-color: #0e5077;
                    color: white
                }

                .services {
                    width: 100%;
                    margin-bottom: 30px;
                }

                .services_left {
                    width: 65%;
                    float: left;
                }

                .services_right {
                    width: 35%;
                    float: right;
                    text-align: center;
                    margin-top: 10px
                }

                .services_right img {
                    max-width: 90%
                }

                .services h3 {
                    font-size: 30px;
                    margin-bottom: 20px;
                }

                .services p {
                    line-height: 30px
                }

                .services ul {
                    margin-top: 10px;
                    margin-bottom: 10px;
                    margin-left: 30px
                }

                .services ul li {
                    float: left;
                    width: 50%;
                    line-height: 30px;
                    list-style-image: url('images/ricon.png')
                }

                .services a {
                    border: 0px;
                    color: white;
                    background-color: #0e5077;
                    text-decoration: none
                }

                .services_padding {
                    padding: 15px
                }

                @media only screen and (max-width : 767px) {
                    .services_left {
                        width: 100%;
                        float: none
                    }

                    .services_right {
                        width: 100%;
                        text-align: center;
                        float: none
                    }

                    .services ul li {
                        width: 100%
                    }

                    .services_right img {
                        width: 250px
                    }

                    #development {
                        float: none;
                        width: 100%
                    }
                }

                
                                    .headform {
                                        margin-bottom: 5px;
                                        margin-top: 5px;
                                        font-size: 14px;
                                        font-weight: bold
                                    }

                                    .field3 {
                                        border: 1px solid #bab9b9;
                                        padding: 5px;
                                        width: 250px;
                                        color: #000;
                                        font-size: 15px;
                                    }

                                    .com_btn {
                                        margin: 0px;
                                        padding: 5px 20px;
                                        border: 0px;
                                        background-color: #1b4f6d;
                                        color: #ffffff;
                                        font-size: 16px;
                                        margin-top: 10px
                                    }
                                
            </style>

@endsection

@section('content')

<div id="headline">
        <h1>Web Development</h1>
    </div>

    <div class="content-adj">
          

            <div class="services">
                <div class="services_left">
                    <div class="services_padding">
                        <h3>Website Design & Development</h3>
                        <p>
                            Need your website developed? No worries! Our team can design and develop website for your
                            brand or business.
                            We offer end to end management & web development solution:
                        </p>
                        <ul>
                            <li>Responsive website design</li>
                            <li>SEO friendly code</li>
                            <li>Wordpress, E-commerce etc</li>
                            <li>Easy to use admin portal</li>
                        </ul>

                        <div class="clear"></div>

                        <p style="line-height: 30px; margin-top: 10px; margin-bottom: 10px">Our Web Design / Development
                            packages are given below:</p>

                        <table width="100%"
                            style="text-align: center; margin: 0px auto; font-size: 15px; border: 1px solid #333"
                            border="0" cellspacing="0">


                            <tr style="font-weight: bold; background-color: #0e5077; color: white;">

                                <td width="33%" style="padding: 5px">Static Website</td>

                                <td width="33%">Wordpress Website</td>

                                <td width="33%">Ecommerce Website</td>

                            </tr>


                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">1 Design Concept</td>

                                <td>Upto 2 Design Concepts</td>

                                <td>Upto 3 Design Concepts</td>

                            </tr>


                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">5-7 Pages</td>

                                <td>Unlimited Pages</td>

                                <td>Unlimited Products</td>

                            </tr>

                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">Contact Form</td>

                                <td>Contact Form</td>

                                <td>Contact Form</td>

                            </tr>

                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">HTML, CSS, PHP</td>

                                <td>+ Wordpress Admin</td>

                                <td>+ Opencart Admin</td>

                            </tr>

                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">Free Domain, Hosting, SSL</td>

                                <td style="padding: 5px">Free Domain, Hosting, SSL</td>

                                <td style="padding: 5px">Free Domain, Hosting, SSL</td>

                            </tr>

                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">SAR: 9,999/-</td>

                                <td>SAR 19,999/-</td>

                                <td>SAR 29,999/-</td>

                            </tr>

                            <tr style="background-color:#f1f1f1;">

                                <td style="padding: 5px">
                                    <div class="orderbtn">
                                        <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=14" class="btn">Order Now</a>
                                        <!-- <a href="account/carte791.html?a=add&amp;pid=14" class="btn">Order Now</a> -->
                                        </div>
                                </td>

                                <td>
                                    <div class="orderbtn">
                                        <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=15" class="btn">Order Now</a>
                                        <!-- <a href="account/cartbf6b.html?a=add&amp;pid=15" class="btn">Order Now</a> -->
                                        </div>
                                </td>

                                <td>
                                    <div class="orderbtn">
                                        <a href="https://www.easyhost.pk/account/cart.php?a=add&pid=16" class="btn">Order Now</a>
                                        <!-- <a href="account/cart94c6.html?a=add&amp;pid=16" class="btn">Order Now</a> -->
                                        </div>
                                </td>

                            </tr>


                        </table>

                        <p style="line-height: 30px; margin-top: 10px; margin-bottom: 10px" id="application">If you are
                            looking at custom Web Development Quotation, please fill in the below form:</p>

                        <div id="development">

                            <div id="abc"
                                style="padding: 10px; border: 1px solid #333; margin-bottom: 0px; text-align: center; background: #f1f1f1; margin-top: 10px">

                                <h3>Web Development Query</h3>

                                <p>Need a custom quote? Send us your requirements below:</p>

                                


                                <form action="https://www.easyhost.pk/web-development?submit=true#application"
                                    method="post" style="margin-top: 10px; margin-bottom: 10px">
                                    <p class="headform">Full Name:</p>
                                    <p><input type="text" name="fname" class="field3" value="" /></p>
                                    <p class="headform">Email Address:</p>
                                    <p><input type="email" name="email" class="field3" value="" /></p>
                                    <p class="headform">Mobile Number:</p>
                                    <p><input type="number" name="mobile" class="field3" value="" /></p>
                                    <p class="headform">Company Name: (optional)</p>
                                    <p><input type="text" name="company" class="field3" value="" /></p>
                                    <p class="headform">Required Website Type:</p>
                                    <p><select name="type" class="field3">
                                            <option value="" selected>-- Select Option --</option>
                                            <option value="Static">Static Site</option>
                                            <option value="Wordpress">Wordpress Site</option>
                                            <option value="Ecommerce">Ecommerce Site</option>
                                            <option value="Custom CMS">Custom CMS Site</option>
                                        </select>
                                    </p>
                                    <p class="headform">Need Web Hosting & Domain?</p>
                                    <p><select name="hosting" class="field3">
                                            <option value="" selected>-- Select Option --</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </p>
                                    <p class="headform">Requirements<br>(you can include sample sites below)</p>
                                    <textarea name="details" style="padding: 5px" cols="50" rows="10"></textarea>
                                    <p class="headform">Verification: what is 3 + 5? type below:</p>
                                    <p><input type="hidden" name="verify" value="8" />
                                        <input type="number" name="vernum" class="field3"
                                            placeholder="Your answer here" /></p>
                                    <p><input name="submit" type="submit" id="submit" tabindex="5" class="com_btn"
                                            value="Submit Application" /><br />
                                </form>

                                <p><i>Note: please ensure correct details are filled in to avoid delays.</i></p>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="services_right">
                    <img src="images/web-design.png" alt="web design">
                    <p style="margin-top: 20px"></p>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>


            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection
