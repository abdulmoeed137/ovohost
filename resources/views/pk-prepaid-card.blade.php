@extends('layouts.app')

@section('title', 'PKNIC Prepaid Cards - .COM Domain cards from SR.1800')

@section('styles')
 <style>
                .products {
                    width: 29%;
                    background: #f1f1f1;
                    border: 1px solid #000;
                    padding: 1%;
                    text-align: center;
                    float: left;
                    margin-right: 1%;
                    margin-left: 1%;
                    margin-bottom: 20px;
                    margin-top: 20px;
                }

                .products h4 {
                    border-bottom: 1px solid #000;
                    margin-bottom: 10px;
                    padding-bottom: 10px
                }

                .products p {
                    font-size: 14px
                }

                .products .price {
                    padding: 3px;
                    background-color: #cccccc;
                    margin-bottom: 15px;
                    margin-top: 5px
                }

                .products .purchase {
                    font-weight: bold;
                    background-color: #0c3754;
                    color: white;
                    padding: 5px;
                    text-decoration: none;
                }

                .sold {
                    margin-top: 10px;
                    font-weight: bold;
                    color: red;
                    margin-bottom: 10px;
                }

                .text {
                    text-align: center;
                    margin-bottom: 7px;
                    font-size: 14px;
                }
            </style>
@endsection

@section('content')

<div id="headline">
        <h1>PKNIC Prepaid Cards</h1>
    </div>

    <div class="content-adj">
            <h2 style="font-size: 24px; margin-bottom: 10px; text-align: center">.COM PKNIC Prepaid Card Prices</h2>

            <p class="text">
            OvoHost is offering PKNIC prepaid cards for the most competitive prices in Saudi Arabia. You can buy PKNIC
                prepaid card for the lowest rate of SR.1,800/card.
                Once you complete your purchase, PKNIC cards will be delivered to your email address within 1-2 hours.
            </p>

           

            <div class="products">
                <h4>1 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,500/each</strong></p>
                <p class="price"><strong>Price: SR.2,500/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=17">Order Now</a></p>
                <!-- <p><a href="account/cart7d20.html?a=add&amp;pid=17">Order Now</a></p> -->
                <div class="clear"></div>
            </div>
            <div class="products">
                <h4>2 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,450/each</strong></p>
                <p class="price"><strong>Price: SR.4,900/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=24">Order Now</a></p>
                <!-- <p><a href="account/cart4f90.html?a=add&amp;pid=24">Order Now</a></p> -->
                <div class="clear"></div>
            </div>

            <div class="products">
                <h4>3 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,400/each</strong></p>
                <p class="price"><strong>Price: SR.7,200/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=25">Order Now</a></p>
                <!-- <p><a href="account/cartdb2f.html?a=add&amp;pid=25">Order Now</a></p> -->
                <div class="clear"></div>
            </div>

            <div class="clear"></div>

            <div class="products">
                <h4>5 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,350/each</strong></p>
                <p class="price"><strong>Price: SR.11,750/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=26">Order Now</a></p>
                <!-- <p><a href="account/cart512a.html?a=add&amp;pid=26">Order Now</a></p> -->
                <div class="clear"></div>
            </div>

            <div class="products">
                <h4>10 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,350/each</strong></p>
                <p class="price"><strong>Price: SR.23,000/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=27">Order Now</a></p>
                <!-- <p><a href="account/cartfa83.html?a=add&amp;pid=27">Order Now</a></p> -->
                <div class="clear"></div>
            </div>

            <div class="products">
                <h4>20 PKNIC Card(s)</h4>
                <p>Buy PKNIC digital card(s) to register .COM domain(s) under your PKNIC account.</p>
                <p><strong>SR.2,250/each</strong></p>
                <p class="price"><strong>Price: SR.45,000/- (one-time)</strong></p>
                <p><a href="https://www.easyhost.pk/account/cart.php?a=add&pid=28">Order Now</a></p>
                <!-- <p><a href="account/cart7419.html?a=add&amp;pid=28">Order Now</a></p> -->
                <div class="clear"></div>
            </div>
            <div class="clear"></div>


            <p class="text">PKNIC cards are used to topup your prepaid PKNIC account credits, using these account
                credits you can register .pk domains and renew existing domains</p>

            <p class='text'><a href="{{route('pkdomain')}}">Register .COM Domain With OvoHost</a> | <a
                    href="{{route('host','pk-domain')}}">Get free .COM Domain with Web Hosting</a></p>

            <div class="buttonset">
                <a class="buttonset-a" style="background: #0e5077" href="{{route('AboutUs')}}">ABOUT OVOHOST</a>
                <a class="buttonset-a" style="background: #e68e35" href="{{route('host','web')}}">GET WEB HOSTING FROM US</a>
                <a class="buttonset-a" style="background: #2fb145" href="{{route('contact')}}">CONTACT OVOHOST TEAM</a>
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
        </div>
@endsection