<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::orderBy('category_id')->oldest()->get();
        return view('admin.packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = [
            '1' => Package::where('category_id', 1)->count(),
            '2' => Package::where('category_id', 2)->count(),
        ];
        return view('admin.packages.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'title' => 'required|string|min:4|max:255',
                'category' => 'required|integer|between:1,2',
                // 'position' => 'required|integer',
                'premium' => 'nullable',
                'best' => 'nullable',
                'no' => 'required|array|min:1',
                'no.*' => 'required|integer|min:1',
                'period' => 'required|array|min:1',
                'period.*' => 'required|integer|between:1,4',
                'price' => 'required|array|min:1',
                'price.*' => 'required|numeric|min:0',
                'price' => 'required|array|min:1',
                'price.*' => 'required|numeric|min:0',
                'save' => 'required|array|min:1',
                'save.*' => 'required|numeric|min:0|max:100',
                'link' => 'required|array|min:1',
                'link.*' => 'required|url',
                'domain' => 'required|array|min:1',
                'domain.*' => 'required|integer|between:0,1',
                'text' => 'required|array|min:1',
                'text.*' => 'required|string|min:4',
                'class' => 'required|array|min:1',
                'class.*' => 'nullable|string|min:4',
            ], [], [
                'price.*' => 'price',
                'save.*' => 'saving',
                'link.*' => 'link',
                'domain.*' => 'domian',
                'text.*' => 'feature',
                'class.*' => 'class',
            ]);
            $package = new Package();
            $package->savePackage($validated);

            return response()->json(['message' => 'Package created successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $positions = [
            '1' => Package::where('category_id', 1)->count(),
            '2' => Package::where('category_id', 2)->count(),
        ];
        return view('admin.packages.edit', compact('package', 'positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        try {
            $validated = $request->validate([
                'title' => 'required|string|min:4|max:255',
                'category' => 'required|integer|between:1,2',
                'premium' => 'nullable',
                'best' => 'nullable',
                // 'position' => 'required|integer',
                'no' => 'required|array|min:1',
                'no.*' => 'required|integer|min:1',
                'period' => 'required|array|min:1',
                'period.*' => 'required|integer|between:1,4',
                'price' => 'required|array|min:1',
                'price.*' => 'required|numeric|min:0',
                'save' => 'required|array|min:1',
                'save.*' => 'required|numeric|min:0|max:100',
                'link' => 'required|array|min:1',
                'link.*' => 'required|url',
                'domain' => 'required|array|min:1',
                'domain.*' => 'required|integer|between:0,1',
                'text' => 'required|array|min:1',
                'text.*' => 'required|string|min:4',
                'class' => 'required|array|min:1',
                'class.*' => 'nullable|string|min:4',
            ], [], [
                'price.*' => 'price',
                'save.*' => 'saving',
                'link.*' => 'link',
                'domain.*' => 'domian',
                'text.*' => 'feature',
                'class.*' => 'class',
            ]);
            $package->savePackage($validated);

            return response()->json(['message' => 'Package updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }
}
