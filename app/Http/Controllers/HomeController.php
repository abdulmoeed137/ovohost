<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        $packages = Package::orderBy('category_id')->oldest()->get();
        $categories = collect([1, 2])->mapWithKeys(
            fn ($c) => [
                $c => $packages->filter(fn ($p) => $p->category_id == $c)
            ]
        );
        $periods = ['Days', 'Weeks', 'Months', 'Years'];
        return view('home', compact('categories', 'periods'));
    }

    public function domain()
    {
        return view('domain');
    }
    public function PkDomain()
    {
        return view('pkdomain');
    }

    public function payment()
    {
        return view('payment');
    }

    public function contact()
    {
        return view('contact');
    }
    public function career()
    {
        return view('careers');
    }
    public function AboutUs()
    {
        return view('about-us');
    }
    public function Affiliates()
    {
        return view('affiliates');
    }
    public function PkPrepaidCard()
    {
        return view('pk-prepaid-card');
    }
    public function WebDevelopment()
    {
        return view('web-development');
    }
    public function SEO()
    {
        return view('seo');
    }
    public function WhoisDomain()
    {
        return view('whoisdomain');
    }
    public function Services()
    {
        return view('services');
    }
    public function FAQ()
    {
        return view('faq');
    }
    public function PkHostingCart()
    {
        return view('pkhostingcart');
    }
    public function Support()
    {
        return view('support');
    }

    public function host($type = 'web')
    {
        return view("hosting.$type");
    }
}
