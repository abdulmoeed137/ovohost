<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'features', 'durations', 'category_id', 'is_premium', 'is_checked', 'position'
    ];

    protected $casts = [
        'durations' => 'object',
        'features' => 'object',
    ];

    public function savePackage($data)
    {

        $durations = [];
        $features = [];
        $this->title = $data['title'];
        $this->category_id = $data['category'];
        $this->is_premium = isset($data['premium']);
        $this->is_best = isset($data['best']);

        for ($i = 0; $i < count($data['price']); $i++) {
            $durations[] = [
                'no' => $data['no'][$i],
                'period' => $data['period'][$i],
                'price' => $data['price'][$i],
                'save' => $data['save'][$i],
                'link' => $data['link'][$i],
                'domain' => $data['domain'][$i]
            ];
        }
        for ($i = 0; $i < count($data['text']); $i++) {
            $features[] = [
                'text' => $data['text'][$i],
                'class' => $data['class'][$i] ?: ""
            ];
        }
        $this->durations = $durations;
        $this->features = $features;
        $this->save();
    }

    // duration = price, percent, domain, link
}
