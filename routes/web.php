<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'account/', 'as' => 'account.'], function () {
    Auth::routes();
});

Route::get('/', 'HomeController@home')->name('home');
Route::get('domain', 'HomeController@domain')->name('domain');
Route::get('support', 'HomeController@Support')->name('support');
Route::get('pk-domain', 'HomeController@PkDomain')->name('pkdomain');
Route::get('payment', 'HomeController@payment')->name('payment');
Route::get('contact-us', 'HomeController@contact')->name('contact');
Route::get('hosting', 'HomeController@host')->name('hosting');
Route::get('{type}/hosting', 'HomeController@host')->name('host');
Route::get('career', 'HomeController@career')->name('career');
Route::get('about-us', 'HomeController@AboutUs')->name('AboutUs');
Route::get('affiliates', 'HomeController@Affiliates')->name('affiliates');
Route::get('pk-prepaid-card', 'HomeController@PkPrepaidCard')->name('pkprepaidcard');
Route::get('web-development', 'HomeController@WebDevelopment')->name('webdevelopment');
Route::get('seo', 'HomeController@SEO')->name('seo');
Route::get('Services', 'HomeController@Services')->name('services');
Route::get('WhoisDomain', 'HomeController@WhoisDomain')->name('whoisdomain');
Route::get('faqs', 'HomeController@FAQ')->name('faq');
Route::get('pk-hosting/cart', 'HomeController@PkHostingCart')->name('pkhostingcart');

Route::group(['prefix' => 'admin/', 'as' => 'admin.'], function () {
    Route::middleware('guest:admin')->group(function () {
        Route::get('login', 'Auth\AdminAuthController@showLoginForm')->name('login.form');
        Route::post('login', 'Auth\AdminAuthController@login')->name('login');
    });
    Route::middleware('auth:admin')->group(function () {
        Route::get('logout', 'Auth\AdminAuthController@logout')->name('logout');
        Route::get('', 'AdminController@home')->name('home');
        Route::resource('packages', 'PackageController')->except(['show', 'destroy']);
    });
});
